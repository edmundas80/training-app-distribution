angular.module('training-app', ['training-app.controllers', 'training-app.services', 'training-app.directives', 'ionic', 'ngCookies', 'pascalprecht.translate', 'pouchdb'])

    .config(['$translateProvider', '$stateProvider', '$urlRouterProvider', '$ionicConfigProvider', function ($translateProvider, $stateProvider, $urlRouterProvider, $ionicConfigProvider){
        // ionic
        $ionicConfigProvider.tabs.position('bottom');
        $ionicConfigProvider.views.maxCache(1);

        // routing
        $stateProvider
            .state('tab', {
                url: '/tab',
                abstract: true,
                templateUrl: 'app/views/application-tabs.html'
            })
            .state('splash', {
                url: '/splash',
                templateUrl: 'app/views/splash.html',
                cache: false
            })
            .state('tab.home', {
                url: '/home',
                views: {
                    'tab-home': {
                        templateUrl: 'app/views/home.html',
                        controller: 'HomeController'
                    }
                },
                cache: false
            })
            .state('tab.exercises-summary', {
                url: '/exercises-summary',
                views: {
                    'tab-exercises-summary': {
                        templateUrl: 'app/views/exercises-summary.html',
                        controller: 'ExercisesSummaryController'
                    }
                },
                cache: false
            })
            .state('tab.exercises', {
                url: '/exercises',
                views: {
                    'tab-exercises-summary': {
                        templateUrl: 'app/views/exercises-tabs.html',
                        controller: 'ExerciseTabsController'
                    }
                },
                cache: false
            })
            .state('tab.exercises.level-1', {
                url: '/level-1',
                views: {
                    'tab-exercises-level1': {
                        templateUrl: 'app/views/exercises.html',
                        controller: 'ExerciseListController'
                    }
                },
                params : { level: 1 },
                cache: false
            })
            .state('tab.exercises.level-2', {
                url: '/level-2',
                views: {
                    'tab-exercises-level2': {
                        templateUrl: 'app/views/exercises.html',
                        controller: 'ExerciseListController'
                    }
                },
                params : { level: 2 },
                cache: false
            })
            .state('tab.exercises.level-3', {
                url: '/level-3',
                views: {
                    'tab-exercises-level3': {
                        templateUrl: 'app/views/exercises.html',
                        controller: 'ExerciseListController'
                    }
                },
                params : { level: 3 },
                cache: false
            })
            .state('tab.history', {
                url: '/history',
                views: {
                    'tab-history': {
                        templateUrl: 'app/views/history.html',
                        controller: 'HistoryController'
                    }
                },
                cache: false
            })
            .state('tab.history-details', {
                url: '/history/details/:id',
                views: {
                    'tab-history': {
                        templateUrl: 'app/views/history-details.html',
                        controller: 'HistoryDetailsController'
                    }
                },
                cache: false
            });

        $urlRouterProvider.otherwise('/splash');

        // translations
        $translateProvider
            .useStaticFilesLoader({
                prefix: 'translations/locale-',
                suffix: '.json'
            })
            .useSanitizeValueStrategy('escape')
            .preferredLanguage('lt')
            .useLocalStorage();
    }])
    .run(['$q', '$log', '$ionicPlatform', 'DatabaseMigration', '$state', '$ionicHistory', function($q, $log, $ionicPlatform, migrationService, $state, $ionicHistory) {
        $ionicPlatform.ready(function() {
            if(window.cordova && window.cordova.plugins.Keyboard) {
              cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
              cordova.plugins.Keyboard.disableScroll(true);
            }

            if(window.StatusBar) {
              StatusBar.styleDefault();
            }

            migrationService.executeDbMigration()
                .then($ionicHistory.clearCache)
                .then($ionicHistory.clearHistory)
                .then(function(){
                    $ionicHistory.nextViewOptions({
                        disableAnimate: true,
                        disableBack: true,
                        historyRoot: true
                    });

                    $state.go('tab.home');
                })
                .catch(function(error){
                    $log.error(error);
                });
        });
}]);

angular.module('training-app')
    .constant('moment', moment)
    .constant('locale', "lt")
    .constant('DB_NAME', 'training-db')
    .constant('SUCCESS_RATES', [
        ['cry.png', 'agressive.png', 'grimace.png'],
        ['cool.png', 'cant-believe-it.png', 'wink.png'],
        ['gape.png', 'glad.png', 'lol.png'],
        ['cheer.png', 'smile.png', 'muhaha.png']
    ]);
angular.module('training-app.services', []);
angular.module('training-app.services')
    .factory('Repository', function(pouchDB, DB_NAME, $q){
        var db = pouchDB(DB_NAME);
        var repository = {};
        repository.db = db;

        repository.all = function(options){
            options = options || {};
            var o = {include_docs: true};

            if (options.startKey){
                o.startkey = options.startKey;
            }

            if (options.endKey){
                o.endkey = options.endKey;
            }

            if (options.pageSize){
                o.limit = options.pageSize;
            }

            if (options.skip){
                o.skip = options.skip;
            }

            if (options.descending){
                o.descending = options.descending;
            }

            return db.allDocs(o)
                .then(function(docs){
                    var found = docs.rows.map(function(value){
                        return value.doc;
                    });

                    var last = (docs.rows && docs.rows.length > 0) ? docs.rows[docs.rows.length - 1].doc._id : null;

                    return { docs: found, lastId: last };
                });
        };

        repository.get = function(id){
            if (!id){
                return $q.reject("id is not specified");
            }

            return db.get(id);
        };

        repository.getSafe = function(id, createDocFn){
            if (!id){
                return $q.reject("id is not specified");
            }

            if (!createDocFn){
                return $q.reject("createDocFn is not specified");
            }

            return repository.exists(id)
                .then(function(exists){
                    if (exists){
                        return db.get(id);
                    }

                    return createDocFn();
                });
        };

        repository.exists = function(id){
            if (!id){
                return $q.reject("id is not specified");
            }

            return db.get(id)
                .then(function(doc){
                    return !!doc;
                })
                .catch(function(error){
                    if (error && error.error && error.message == 'missing'){
                        return false;
                    }

                    throw error;
                });
        };

        repository.saveOrUpdate = function(doc){
            return $q.when(doc)
                .then(function(doc){
                    if (!doc || !doc._id) {
                        return $q.reject("No document with _id");
                    }

                    return doc;
                })
                .then(function(doc){
                    return db.get(doc._id)
                        .then(function(docFromDb){
                            doc._rev = docFromDb._rev;
                            return db.put(doc);
                        });
                })
                .catch(function(error){
                    if (error && error.error && error.message == 'missing'){
                        return db.put(doc);
                    }

                    throw error;
                });
        };

        repository.remove = function(id){
            if (!id){
                return $q.reject("id is not specified");
            }

            return repository.get(id)
                .then(function(doc){
                    return db.remove(doc);
                });
        };

        return repository;
    });

angular.module('training-app.services')
    .service('DateTimeService', ['moment', 'locale', function(moment, locale){
        var self = this;

        self.toLongDate = function(unixDate){
            moment.locale(locale);

            var trainingDate = moment.unix(unixDate);
            return trainingDate.format("LLL");
        };

        self.toDuration = function(durationInSeconds){
            moment.locale(locale);

            return moment.duration(durationInSeconds, "seconds").humanize();
        };
    }]);
angular.module('training-app.services')
    .constant('DB_VERSION_DOC_ID', '5e60ddd0-fdfe-4f1f-b6e8-9a344dcde9ef')
    .service('DatabaseMigration', ['Repository', 'DB_VERSION_DOC_ID', '$http', '$q', '$log', function(repository, dbVersionDocId, $http, $q, $log){
        var db = repository.db;

        this.executeDbMigration = function(){
            return this._getDbVersion()
                .then(this._updateDb);
        };

        this._getDbVersion = function(){
            return db.get(dbVersionDocId)
                .then(function(doc){
                    return 0;
                    //return doc.version;
                })
                .catch(function(error){
                    if (error && error.error && error.message == 'missing'){
                        return 0;
                    }

                    throw error;
                });
        };

        this._updateDb = function(dbVersion){
            return $http.get('app/data.json')
                .then(function(res){
                    var appDocuments = res.data;
                    var needsUpdate;

                    angular.forEach(appDocuments, function(doc){
                        if (doc._id == dbVersionDocId && doc.version > dbVersion){
                            needsUpdate = true;
                        }
                    });

                    if (needsUpdate){
                        return $q.when((function(){
                            return appDocuments.map(function(appDocument){
                                return appDocument._id;
                            });
                        })())
                        .then(function(ids){
                            return db.allDocs({keys: ids});
                        })
                        .then(function(docsToDelete){
                            if (docsToDelete.rows > []){
                                return $q.all(docsToDelete.rows.map(function(doc){
                                    if (!doc.error && !doc.value.deleted){
                                        return db.remove(doc.id, doc.value.rev);
                                    }
                                }));
                            }
                        })
                        .then(function(){
                            return db.bulkDocs(appDocuments);
                        });
                    }
                });
        };
    }]);
angular.module('training-app.services')
.config(function($provide){
    $provide.decorator("$log", function($delegate) {
        var errorFn = $delegate.error;
        $delegate.error = function(message) {
            if (ionic.Platform.isWebView()){
                alert(message);
            }

            errorFn.apply(null, arguments);
        };
        return $delegate;
    });
});

angular.module('training-app.services')
    .constant('EXERCISE_DRAFT_DOC_ID', '9bbcc844-0376-4f40-b4ab-c34cc048775c')
    .constant('EXERCISE_SUMMARY_DOC_ID', '35b623a1-7fd3-4543-928a-d2a69303949e')
    .service('ExerciseService', ['Repository', '$q', 'filterFilter', '$filter', 'EXERCISE_DRAFT_DOC_ID', 'EXERCISE_SUMMARY_DOC_ID', function(repository, $q, filterFilter, $filter, EXERCISE_DRAFT_DOC_ID, EXERCISE_SUMMARY_DOC_ID){
        var self = this;
        var variations = null;

        self.getExercises = function(level){
            return repository.all()
                .then(function(result){
                    var docs = result.docs;
                    var filter = {type: 'exercise'};

                    if (level){
                        filter.stage = level;
                    }

                    return filterFilter(docs, filter);
                })
                .then(function(exercises){
                    return $filter('orderBy')(exercises, 'name');
                });
        };

        self.getExerciseVariations = function(){
            if (variations){
                return $q.when(variations);
            }

            return repository.all()
                .then(function(result){
                    var exercises = result.docs;
                    var result = [];

                    angular.forEach(exercises, function(exercise){
                        angular.forEach(exercise.variations, function(variation){
                            var e = angular.copy(exercise);
                            e.productName = e.name;
                            e.name = variation.name;
                            e.description = variation.description;
                            delete e.variations;
                            result[variation._id] = e;
                        });
                    });

                    variations = result;
                    return result;
                });
        };

        self.getExerciseConfigurationDraft = function(){
            return repository.getSafe(EXERCISE_DRAFT_DOC_ID, self._createExerciseConfigurationDraft)
                .then(function(doc){
                    return doc;
                });
        };

        self.saveExerciseConfigurationDraft = function(exerciseVariations, level){
            if (!exerciseVariations){
                return $q.reject("exerciseVariations is not specified");
            }

            if (level == null || !(level >= 1 && level <= 3)){
                return $q.reject("level is invalid or not specified");
            }

            var variations = exerciseVariations.map(function(variation) {
                return variation._id;
            });

            return repository.getSafe(EXERCISE_DRAFT_DOC_ID, self._createExerciseConfigurationDraft)
                .then(function(doc){
                    doc.level[level - 1] = variations;
                    doc.dirty = true;

                    return repository.saveOrUpdate(doc);
                });
        };

        self.removeExerciseConfigurationDraft = function(){
            return repository.getSafe(EXERCISE_DRAFT_DOC_ID, function(){ return null; })
                .then(function(doc){
                    if (doc){
                        return repository.db.remove(EXERCISE_DRAFT_DOC_ID, doc._rev);
                    }
                });
        };

        self.getExerciseSummary = function(){
            return repository.getSafe(EXERCISE_SUMMARY_DOC_ID, self._createExerciseSummary)
                .then(function(doc){
                    return doc;
                });
        };

        self.saveExerciseSummaryFromDraft = function(draft){
            if (!draft || !draft.level){
                return $q.reject("invalid draft specified");
            }

            return repository.getSafe(EXERCISE_SUMMARY_DOC_ID, self._createExerciseSummary)
                .then(function(doc){
                    var draftExercises = [].concat([].concat(draft.level[0], draft.level[1]), draft.level[2]);
                    var storedExercises = doc.exercises;
                    var index;

                    // Summary exercise does not exists in draft - delete it from summary, it is not configured anymore
                    for (index = storedExercises.length - 1; index >= 0 ; index--){
                        if (draftExercises.indexOf(storedExercises[index]) == -1){
                            storedExercises.splice(index, 1);
                        }
                    }

                    // Draft exercise does exist is summary - delete it from draft to maintain summary exercise position
                    for (index = draftExercises.length - 1; index >= 0 ; index--){
                        if (storedExercises.indexOf(draftExercises[index]) !== -1){
                            draftExercises.splice(index, 1);
                        }
                    }

                    doc.exercises = [].concat(draftExercises, storedExercises);
                    return repository.saveOrUpdate(doc);
                });
        };

        self.saveExerciseSummaryExercises = function(exercises){
            return repository.getSafe(EXERCISE_SUMMARY_DOC_ID, self._createExerciseSummary)
                .then(function(doc){
                    doc.exercises = exercises;
                    return repository.saveOrUpdate(doc);
                });
        };

        self._deleteDatabase = function(){
            return repository.db.destroy();
        };

        self._createExerciseConfigurationDraft = function(){
            var result = {
                _id: EXERCISE_DRAFT_DOC_ID,
                type: "exerciseConfigurationDraft",
                level: [[], [], []],
                dirty: false
            };

            return repository.getSafe(EXERCISE_SUMMARY_DOC_ID, self._createExerciseSummary)
                .then(function(summaryDoc){
                    if (summaryDoc.exercises > []){
                        var summaryExercises = summaryDoc.exercises;
                        return self.getExerciseVariations()
                            .then(function(variations){
                                angular.forEach(summaryExercises, function(exercise){
                                    if (variations[exercise]){
                                        result.level[variations[exercise].stage - 1].push(exercise);
                                    }
                                });

                                return result;
                            });
                    }

                    return result;
                });
        };

        self._createExerciseSummary = function(){
            return {
                _id: EXERCISE_SUMMARY_DOC_ID,
                type: "exerciseSummary",
                exercises: []
            };
        };
    }]);
angular.module('training-app.services')
    .constant('ACTIVITY_DOC_ID', '60e5d7ef-e128-437e-8eab-f9a989a38caa')
    .service('ActivityService', ['Repository', 'ExerciseService', '$q', 'filterFilter', '$filter', 'ACTIVITY_DOC_ID', 'moment', '$log', function(repository, exerciseService, $q, filterFilter, $filter, ACTIVITY_DOC_ID, moment, $log){
        var self = this;

        self.getActivity = function(){
            $log.log("ActivityService.getActivity");

            return repository.getSafe(ACTIVITY_DOC_ID, self._createActivity)
                .then(function(activity){
                    return (!activity.inProgress) ? self._setFirstAvailableExercise(activity) : activity;
                });
        };

        self.getTotalExercisesForActivity = function(){
            $log.log("ActivityService.getTotalExercisesForActivity");

            var summaryExercises = [];

            return exerciseService.getExerciseSummary()
                .then(function(summary){
                    summaryExercises = summary.exercises;
                })
                .then(self.getActivity)
                .then(function(activity){
                    if (activity.currentExercise && summaryExercises.indexOf(activity.currentExercise) < 0){
                        summaryExercises.push(activity.currentExercise);
                    }

                    var i;
                    for (i = 0; i < activity.completedExercises.length; i++){
                        var exercise = activity.completedExercises[i];

                        if (summaryExercises.indexOf(exercise._id) < 0){
                            summaryExercises.push(exercise._id);
                        }
                    }

                    return { total: summaryExercises.length, activity: activity };
                });
        };

        self.startActivity = function(){
            $log.log("ActivityService.startActivity");

            return self.getActivity()
                .then(function(activity){
                    if (activity.suspended){
                        return self.resumeActivity();
                    }

                    if (activity.inProgress){
                        return activity;
                    }

                    if (activity.currentExercise){
                        activity.inProgress = true;
                        activity.started = true;
                        activity.lastStartTime = moment().unix();
                        return activity;
                    }

                    return $q.reject("no exercise to start");
                })
                .then(function(activity){
                    return repository.saveOrUpdate(activity)
                        .then(function(){
                            $log.log(activity);
                            return activity;
                        })
                });
        };

        self.stopActivity = function(){
            $log.log("ActivityService.stopActivity");

            return self.getActivity()
                .then(function(activity){
                    if (!activity.inProgress){
                        return $q.reject("activity is not started");
                    }

                    if (!activity.currentExercise){
                        return $q.reject("no current exercise set");
                    }

                    var savedExercise = filterFilter(activity.completedExercises, {_id: activity.currentExercise});

                    if (savedExercise.length > 0){
                        return $q.reject("current exercise is already completed");
                    }

                    activity.inProgress = false;

                    if (!activity.suspended){
                        activity.duration = moment().unix() - activity.lastStartTime + activity.duration;
                    }

                    activity.completedExercises.push({_id: activity.currentExercise, duration: activity.duration});
                    activity.currentExercise = null;
                    activity.lastStartTime = 0;
                    activity.duration = 0;
                    activity.suspended = false;

                    return activity;

                })
                .then(function(activity){
                    return repository.saveOrUpdate(activity)
                        .then(function(){
                            $log.log(activity);
                            return activity;
                        })
                });
        };

        self.suspendActivity = function(autostart){
            $log.log("ActivityService.suspendActivity");

            return self.getActivity()
                .then(function(activity){
                    if (!activity.inProgress){
                        return $q.reject("activity is not started");
                    }

                    if (!activity.currentExercise){
                        return $q.reject("no current exercise set");
                    }

                    if (activity.suspended){
                        return $q.reject("activity is already suspended");
                    }

                    activity.autostart = !!autostart;
                    activity.suspended = true;

                    if (!activity.autostart){
                        activity.duration = moment().unix() - activity.lastStartTime + activity.duration;
                        activity.lastStartTime = 0;
                    }

                    return activity;

                })
                .then(function(activity){
                    return repository.saveOrUpdate(activity)
                        .then(function(){
                            $log.log(activity);
                            return activity;
                        })
                });
        };

        self.resumeActivity = function(){
            $log.log("ActivityService.resumeActivity");

            return self.getActivity()
                .then(function(activity){
                    if (!activity.inProgress){
                        return $q.reject("activity is not started");
                    }

                    if (!activity.currentExercise){
                        return $q.reject("no current exercise set");
                    }

                    if (!activity.suspended){
                        return $q.reject("activity is not suspended");
                    }

                    activity.suspended = false;

                    if (activity.autostart){
                        activity.duration = moment().unix() - activity.lastStartTime + activity.duration;
                    }

                    activity.lastStartTime = moment().unix();
                    activity.autostart = false;

                    return activity;

                })
                .then(function(activity){
                    return repository.saveOrUpdate(activity)
                        .then(function(){
                            $log.log(activity);
                            return activity;
                        })
                });
        };

        self.revertActivity = function(){
            $log.log("ActivityService.revertActivity");

            return self.getActivity()
                .then(function(activity){
                    if (activity.inProgress){
                        return $q.reject("activity is already started");
                    }

                    if (activity.completedExercises.length == 0){
                        return $q.reject("no completed exercises");
                    }

                    var last = activity.completedExercises.pop();

                    activity.inProgress = false;
                    activity.duration = last.duration;
                    activity.currentExercise = last._id;
                    activity.lastStartTime = 0;

                    return activity;
                })
                .then(function(activity){
                    return repository.saveOrUpdate(activity)
                        .then(function(){
                            $log.log(activity);
                            return activity;
                        })
                });
        };

        self.rateLastActivity = function(rating1, rating2){
            $log.log("ActivityService.rateLastActivity");

            if (rating1 < 0 || rating1 > 4){
                return $q.reject("invalid rating1");
            }

            if (rating2 < 0 || rating2 > 2){
                return $q.reject("invalid rating2");
            }

            return self.getActivity()
                .then(function(activity){
                    if (activity.completedExercises.length == 0){
                        return $q.reject("no completed exercises");
                    }

                    var last = activity.completedExercises[activity.completedExercises.length - 1];

                    if (last.rating){
                        return $q.reject("exercise is already rated");
                    }

                    last.rating = {rating1: rating1, rating2: rating2};
                    return activity;
                })
                .then(function(activity){
                    return repository.saveOrUpdate(activity)
                        .then(function(){
                            $log.log(activity);
                            return activity;
                        })
                })
        };

        self.removeActivity = function(){
            return repository.remove(ACTIVITY_DOC_ID);
        }

        self._createActivity = function(){
            return {
                _id: ACTIVITY_DOC_ID,
                type: "activity",
                started: false,
                inProgress: false,
                suspended: false,
                autostart: false,
                lastStartTime: 0,
                duration: 0,
                currentExercise: null,
                completedExercises: []
            };
        };

        self._setFirstAvailableExercise = function(activity){
            return exerciseService.getExerciseSummary()
                .then(function(summary){
                    var i;

                    for (i = 0; i < summary.exercises.length; i++){
                        var exercise = summary.exercises[i];
                        var item = filterFilter(activity.completedExercises, {_id: exercise });

                        if (item.length == 0){
                            activity.currentExercise = exercise;
                            break;
                        }
                    }

                    return activity;
                })
        }
    }]);
angular.module('training-app.services')
    .constant("MIN_HISTORY_ID", "activityHistory_1")
    .constant("MAX_HISTORY_ID", "activityHistory_9999999999")
    .constant("PAGE_SIZE", 10)
    .service('ActivityHistoryService', ['Repository', 'ExerciseService', 'ActivityService', '$q', 'filterFilter', '$filter', 'moment', '$log', 'MIN_HISTORY_ID', 'MAX_HISTORY_ID', 'PAGE_SIZE', function(repository, exerciseService, activityService, $q, filterFilter, $filter, moment, $log, MIN_HISTORY_ID, MAX_HISTORY_ID, PAGE_SIZE){
        var self = this;

        self.saveActivityHistory = function(activity){
            $log.log("ActivityHistoryService.saveActivityHistory");

            if (!activity){
                return $q.reject("no activity set");
            }

            if (activity.completedExercises.length == 0) {
                return $q.reject("no completed exercises");
            }

            var historyId = "activityHistory_" + moment().unix();
            var history = self._createActivityHistory(historyId);

            history.historyDate = moment().unix();
            history.exerciseCount = activity.completedExercises.length;
            history.completedExercises = activity.completedExercises;

            angular.forEach(activity.completedExercises, function(exercise){
                history.duration += exercise.duration;
            });

            return repository.saveOrUpdate(history)
                .then(function(){
                    $log.log(history);
                    return history;
                })
        };

        self.getList = function(lastId){
            lastId = lastId || MAX_HISTORY_ID;

            var options = {startKey: lastId, pageSize: PAGE_SIZE, descending: true};

            if (lastId != MAX_HISTORY_ID){
                options.skip = 1;
            }

            return repository.all(options)
                .then(function(result){
                    var filter = {type: 'activityHistory'};
                    var historyItems = filterFilter(result.docs, filter);
                    var lastId = (historyItems.length > 0) ? historyItems[historyItems.length - 1]._id : null;
                    var r = {docs: historyItems, lastId: lastId};

                    $log.log(r);
                    return r;
                })
        };

        self.get = function(id){
            if (!id){
                return $q.reject("no id set");
            }

            return repository.get(id);
        };

        self._createActivityHistory = function(id){
            return {
                _id: id,
                type: "activityHistory",
                duration: 0,
                exerciseCount: 0,
                historyDate: null,
                completedExercises: []
            };
        };
    }]);
angular.module('training-app.services')
    .service('ModalService', ['$ionicModal', function($ionicModal){
        var self = this;

        self.showModal = function(templateUrl, scope){
            $ionicModal.fromTemplateUrl(templateUrl, {
                scope: scope,
                animation: 'animated bounceIn',
                hideDelay: 920,
                backdropClickToClose: false,
                hardwareBackButtonClose: false
            }).then(function(modal) {
                scope.modal = modal;
                scope.modal.show();
                scope.hideModal = function(){
                    if (scope.modal){
                        scope.modal.hide();
                        scope.modal.remove();
                        scope.modal = null;
                    }
                }
            });
        }
    }]);
angular.module('training-app.directives', []);
angular.module('training-app.directives')
    .directive('closeButton', function() {
        return {
            restrict: 'E',
            replace: true,
            template: '<a href="" class="button button-icon"></a>',
            link: function(scope, element){
                if (ionic.Platform.isAndroid()){
                    element.addClass("ion-android-close");
                }
                else {
                    element.addClass("ion-ios-close-empty");
                }
            }
        };
    });
angular.module('training-app.directives')
    .directive('tooltip', function($ionicPopover) {
        return {
            restrict: 'E',
            replace: true,
            template: '<a href="" class="padding-right" style="pointer-events: auto"></a>',
            scope: {
                text: '=',
                exerciseTitle: '=',
                iconOnAttr: '@iconOn',
                iconOffAttr: '@iconOff'
            },
            link: function(scope, element, attributes){
                scope.iconOn = scope.iconOnAttr || "ion-ios-information";
                scope.iconOff = scope.iconOffAttr || "ion-ios-information-outline";

                var template = '<ion-popover-view>' +
                    '     <ion-header-bar> ' +
                    '          <h1 class="title">{{"HELP_ADDITIONAL_INFO" | translate }}</h1> '+
                    '             <div class="buttons"> '+
                    '                 <close-button ng-click="close()"></close-button>' +
                    '             </div>' +
                    '     </ion-header-bar> ' +
                    '     <ion-content class="padding"> <h5> {{exerciseTitle}} </h5> {{text}} </ion-content>' +
                    '</ion-popover-view>';
                var popover = $ionicPopover.fromTemplate(template, {
                    scope: scope
                });

                element.addClass(scope.iconOff);
                element.on('click', function(event) { scope.open(event);});

                scope.close = function(){
                    element.removeClass(scope.iconOn);
                    element.addClass(scope.iconOff);

                    popover.hide();
                };

                scope.open = function(event){
                    element.removeClass(scope.iconOff);
                    element.addClass(scope.iconOn);

                    popover.show(event);
                };

                scope.$on('popover.hidden', function() {
                    element.removeClass(scope.iconOn);
                    element.addClass(scope.iconOff);
                });
            }
        };
    });
angular.module('training-app.directives')
    .directive('exercisesMoreMenu', function($ionicPopover) {
        return {
            restrict: 'E',
            replace: true,
            template: '<a href="" class="button button-icon icon ion-android-more-vertical"></a>',
            scope: {
                onDiscard: '&'
            },
            link: function(scope, element){
                var template =
                    '<ion-popover-view class="fit">'+
                    '   <ion-content>'+
                    '       <div class="list">'+
                    '           <a class="item" href="" ng-click="discard()">{{"DISCARD" | translate}}</a>'+
                    '        </div>'+
                    '   </ion-content>'+
                    '</ion-popover-view>';

                var popover = $ionicPopover.fromTemplate(template, { scope: scope });

                element.on('click', function(event) {popover.show(event);});

                scope.discard = function(){
                    scope.onDiscard();
                    popover.hide();
                };

                scope.$on('$destroy', function() {
                    popover.remove();
                });
            }
        };
    });
angular.module('training-app.directives')
    .directive('staticTimer', function(moment) {
        return {
            restrict: 'E',
            replace: true,
            template: '<span></span>',
            scope: {
                startTime: '='
            },
            link: function(scope, element){
                scope.$watch('startTime', function(newValue) {
                    if (newValue){
                        var millis = moment().diff(moment(newValue));

                        var seconds = Math.floor((millis / 1000) % 60);
                        var minutes = Math.floor(((millis / (60000)) % 60));
                        var hours = Math.floor(millis / 3600000);

                        var sseconds = seconds < 10 ? '0' + seconds : '' + seconds;
                        var mminutes = minutes < 10 ? '0' + minutes : '' + minutes;
                        var hhours = hours < 10 ? '0' + hours : '' + hours;

                        element.text(hhours + ':' + mminutes + ':' + sseconds);
                    }
                }, true);
            }
        };
    });
angular.module('training-app.controllers', ['angular-svg-round-progressbar', 'timer']);
angular.module('training-app.controllers')
    .value("ACTIVITY_TRAINING_NOT_STARTED", 0)
    .value("ACTIVITY_TRAINING_IN_PROGRESS", 1)
    .value("ACTIVITY_TRAINING_COMPLETED", 2)
    .controller('HomeController', ['$scope', '$rootScope', '$ionicPlatform', '$ionicPopover', '$log', '$timeout', '$document', 'ModalService', 'SUCCESS_RATES', 'ExerciseService', 'ActivityService', 'ActivityHistoryService', 'Repository', 'ACTIVITY_TRAINING_NOT_STARTED', 'ACTIVITY_TRAINING_IN_PROGRESS', 'ACTIVITY_TRAINING_COMPLETED', 'ACTIVITY_DOC_ID', function($scope, $rootScope, $ionicPlatform, $ionicPopover, $log, $timeout, $document, modalService, SUCCESS_RATES, exerciseService, activityService, activityHistoryService, repository, ACTIVITY_TRAINING_NOT_STARTED, ACTIVITY_TRAINING_IN_PROGRESS, ACTIVITY_TRAINING_COMPLETED, ACTIVITY_DOC_ID) {
        var self = this;

        $scope.successRates =  SUCCESS_RATES;
        $scope.startedAfterInit = false;
        $scope.isPlayOn = false;
        $scope.isStopOn = true;
        $scope.isPauseOn = false;
        $scope.mode = ACTIVITY_TRAINING_NOT_STARTED;
        $scope.modeStr = "";
        $scope.variations = {};
        $scope.total = -1;
        $scope.current = 0;
        $scope.startTime = new Date();
        $scope.isMenuVisible = false;

        $scope.$watch("mode", function(value){
            if (value == ACTIVITY_TRAINING_NOT_STARTED){
                $scope.modeStr = "ACTIVITY_TRAINING_NOT_STARTED";
            } else if (value == ACTIVITY_TRAINING_IN_PROGRESS){
                $scope.modeStr = "ACTIVITY_TRAINING_IN_PROGRESS";
            } else {
                $scope.modeStr = "ACTIVITY_TRAINING_COMPLETED";
            }
        }, true);

        $scope.$on("$ionicView.loaded", function(){
            self.init()
                .catch(function(error){
                    $log.error(error);
                });
        });

        var stateChangeListener = $rootScope.$on('$stateChangeStart', function(){
            $log.log('$stateChangeStart');

            activityService.getActivity()
                .then(function(activity){
                    if (activity.inProgress && !activity.suspended){
                        return activityService.suspendActivity(true);
                    }
                })
                .then(function(){
                    $scope.$broadcast('timer-stop');
                })
                .catch(function(error){
                    $log.error(error);
                });

            stateChangeListener()
        });

        var pauseListener = $ionicPlatform.on('pause', function() {
            activityService.getActivity()
                .then(function(activity){
                    if (activity.inProgress && !activity.suspended){
                        return activityService.suspendActivity(true);
                    }
                })
                .then(function(){
                    $scope.$broadcast('timer-stop');
                })
                .catch(function(error){
                    $log.error(error);
                });
        });

        var resumeListener = $ionicPlatform.on('resume', function() {
            setTimeout(function(){
                activityService.getActivity()
                    .then(function(activity){
                        if (activity.suspended && activity.autostart){
                            return $scope.play();
                        }
                    })
                    .catch(function(error){
                        $log.error(error);
                    });
            }, 1000);
        });


        $scope.$on('$destroy', function() {
            resumeListener();
            pauseListener();

            $scope.popover.remove();
        });

        $scope.play = function(){
            $scope.isPlayOn = true;
            $scope.isStopOn = $scope.isPauseOn = false;

            return activityService.startActivity()
                .then(function(activity){
                    self.setStartTime(activity);
                    $scope.$broadcast('timer-resume');
                    $scope.mode = ACTIVITY_TRAINING_IN_PROGRESS;
                    $scope.startedAfterInit = true;
                })
                .then(self.getMenuVisibility)
                .catch(function(error){
                    $log.error(error);
                });
        }

        $scope.stop = function(){
            $scope.isStopOn = true;
            $scope.isPlayOn = $scope.isPauseOn = false;

            return activityService.stopActivity()
                .then(self.getNextActivity)
                .then(function(){
                    $scope.$broadcast('timer-stop');
                    modalService.showModal('app/views/rating-modal.html', $scope);
                })
                .then(self.getMenuVisibility)
                .catch(function(error){
                    $log.error(error);
                });
        }

        $scope.pause = function(){
            $scope.isPauseOn = true;
            $scope.isStopOn = $scope.isPlayOn = false;

            return activityService.suspendActivity()
                .then(function(){
                    $scope.$broadcast('timer-stop');
                })
                .then(self.getMenuVisibility)
                .catch(function(error){
                    $log.error(error);
                })
        }

        $scope.revertToPreviousExercise = function(){
            return activityService.revertActivity()
                .then(function(activity){
                    $scope.currentExercise = activity.currentExercise;
                })
                .then($scope.play)
                .then(function(){
                    $scope.hideModal();
                })
                .catch(function(error){
                    $log.error(error);
                });
        }

        $scope.selectSuccessRate = function($event, index1, index2) {
            var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
            var successRateElement = angular.element($event.currentTarget).children(':first');

            successRateElement.addClass("animated bounce").one(animationEnd, function() {
                successRateElement.removeClass('animated bounce');

                activityService.rateLastActivity(index1, index2)
                    .then(function() {
                        $scope.hideModal();
                        $scope.current++;
                    })
                    .then(activityService.getActivity)
                    .then(function(activity){
                        if ($scope.total == $scope.current){
                            $scope.mode = ACTIVITY_TRAINING_COMPLETED;

                            return activityHistoryService.saveActivityHistory(activity);
                        }
                    })
                    .catch(function(error){
                        $log.error(error);
                    });
            });
        };

        $scope.newTraining = function(){
            activityService.getActivity()
                .then(function(activity){
                    return activityService.removeActivity();
                })
                .then(function(){
                    return self.init();
                })
                .then(function(){
                    $scope.startedAfterInit = false;
                    $scope.isPlayOn = false;
                    $scope.isStopOn = true;
                    $scope.isPauseOn = false;
                    $scope.mode = ACTIVITY_TRAINING_NOT_STARTED;
                })
                .catch(function(error){
                    $log.error(error);
                })
                .finally(function(){
                    $scope.popover.hide();
                    $scope.startedAfterInit = false;
                });
        };

        $scope.openMenu = function($event){
            $log.log('click', $event);
            $scope.popover.show($event);
        };

        self.init = function(){
            if (!$scope.popover){
                $ionicPopover.fromTemplateUrl('home-menu', {
                    scope: $scope
                }).then(function(popover) {
                    $scope.popover = popover;
                });
            }

            $log.log("Initialize activity data.");

            return self.getNextActivity()
                .then(self.getMenuVisibility)
                .then(exerciseService.getExerciseVariations)
                .then(function(variations){
                    $scope.variations = variations;
                })
                .then(activityService.getTotalExercisesForActivity)
                .then(function(result){
                    var activity = result.activity;

                    self.setStartTime(activity);

                    $scope.current = activity.completedExercises.length;
                    $scope.total = result.total;

                    $log.log('Current exercise', $scope.currentExercise);
                    $log.log('Variations', $scope.variations);

                    var autostart = activity.autostart || (activity.inProgress && !activity.suspended);

                    if (autostart){
                        if (ionic.Platform.isAndroid()){
                            $scope.play();
                        }
                        else {
                            // iOS app does not fire pause event before being killed from the app list
                            if (activity.inProgress && !activity.suspended){
                                activityService.suspendActivity(true).then($scope.play);
                            }
                            else {
                                $scope.play();
                            }
                        }
                    }

                    $scope.isPauseOn = activity.suspended && !autostart;

                    if ($scope.total == $scope.current){
                        $scope.mode = ACTIVITY_TRAINING_COMPLETED;
                    } else if (activity.started){
                        $scope.mode = ACTIVITY_TRAINING_IN_PROGRESS;
                    } else {
                        $scope.mode = ACTIVITY_TRAINING_NOT_STARTED;
                    }
                });
        };

        self.getNextActivity = function(){
            return activityService.getActivity()
                .then(function(activity){
                    $log.log("Activity data received.");
                    $scope.currentExercise = activity.currentExercise;
                    return activity;
                });
        };

        self.setStartTime = function(activity){
            var i;
            var completedExerciseDuration = 0;

            for (i = 0; i < activity.completedExercises.length; i++){
                completedExerciseDuration += activity.completedExercises[i].duration;
            }

            if (activity.autostart){
                $scope.startTime = new Date() - (moment().unix() - activity.lastStartTime + activity.duration + completedExerciseDuration) * 1000;
            } else {
                $scope.startTime = new Date() - (activity.duration + completedExerciseDuration) * 1000;
            }
        };

        self.getMenuVisibility = function(){
            return repository.exists(ACTIVITY_DOC_ID)
                .then(function(activityDocumentExists){
                    $scope.isMenuVisible = activityDocumentExists;
                });
        }
    }]);
angular.module('training-app.controllers')
    .controller('ExerciseTabsController', function($scope, $log) {});
angular.module('training-app.controllers')
    .controller('ExerciseListController', ['$scope', '$rootScope', '$stateParams', '$log', '$state', 'ExerciseService', function($scope, $rootScope, $stateParams, $log, $state, exerciseService) {
        var self = this;

        $scope.level = $stateParams.level;
        $scope.forms = {};
        $scope.items = [];
        $scope.dirty = false;
        $scope.storedVariations = [];

        $scope.$watch('forms.exerciseForm.$dirty', function(newVal){
            if (newVal){
                $scope.dirty = newVal;
            }
        });

        $scope.$on("$ionicView.loaded", function(){
            self.init()
                .catch(function(error){
                    $log.error(error);
                });;
        });

        $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
            viewData.enableBack = true;
        });

        var stateChangeListener = $rootScope.$on('$stateChangeStart', function(event, toState, toParams){
            self.navigateToState(event, toState, toParams);
            stateChangeListener();
        });

        $scope.save = function(){
            $log.log("Save started");

            return self.saveDraft()
                .then(exerciseService.getExerciseConfigurationDraft)
                .then(exerciseService.saveExerciseSummaryFromDraft)
                .then(function(){
                    return $state.go("tab.exercises-summary");
                })
                .catch(function(error){
                    $log.error(error);
                });
        };

        $scope.discard = function(){
            $log.log("Discard started");
            return $state.go("tab.exercises-summary");
        };

        self.init = function(){
            $log.log("Configuration draft view initializing.");

            return exerciseService.getExerciseConfigurationDraft($scope.level)
                .then(function(doc){
                    $log.log("Draft view configuration reading.");
                    $scope.dirty = doc.dirty;
                    $scope.storedVariations = doc.level[$scope.level - 1];
                })
                .then(function(){
                    return exerciseService.getExercises($scope.level);
                })
                .then(function(exercises){
                    angular.forEach(exercises, function(exercise){
                        return angular.forEach(exercise.variations, function(variation){
                            variation.selected = $scope.storedVariations.indexOf(variation._id) >= 0;
                            return variation;
                        });
                    });

                    $log.log("Exercises loaded from DB");
                    $scope.items = exercises;
                });
        };

        self.saveDraft = function(){
            $log.log("Draft save initiated.");

            var selectedVariations = [];

            angular.forEach($scope.items, function(exercise){
                angular.forEach(exercise.variations, function(variation){
                    if (variation.selected){
                        selectedVariations.push(variation);
                    }
                });
            });

            return exerciseService.saveExerciseConfigurationDraft(selectedVariations, $scope.level)
                .then(function(){
                    $scope.dirty = false;
                    $log.log("Draft saved successfully.");
                })
                .catch(function(error){
                    $log.error("Saving configuration failed. ", error);
                });
        };

        self.navigateToState = function(event, toState, toParams){
            $log.log("Navigating to state: ", toState.name);

            if (toState.name != "tab.exercises.level-1" && toState.name != "tab.exercises.level-2" && toState.name != "tab.exercises.level-3"){
                exerciseService.removeExerciseConfigurationDraft()
                    .then(function(){
                        $log.log("Configuration draft deleted. ");
                    })
                    .catch(function(error){
                        $log.error("Configuration draft not deleted. ", error);
                    });
            } else if ($scope.dirty){
                $log.log("Preventing state transition");

                event.preventDefault();

                self.saveDraft()
                    .then(function(){
                        $state.go(toState, toParams);
                    })
                    .catch(function(error){
                        $log.error(error);
                    });

            } else {
                $log.log("Continuing with state transition");
            }
        }
    }]);

angular.module('training-app.controllers')
    .controller('HistoryController', ['$scope', '$log', '$q', '$state', 'ActivityHistoryService', 'DateTimeService', function($scope, $log, $q, $state, activityHistoryService, dateTimeService) {
        var self = this;

        $scope.lastId = null;
        $scope.historyItems = [];
        $scope.hasMore = true;

        $scope.$on("$ionicView.loaded", function(){
            self.init()
                .then(function(){
                    $log.log($scope.historyItems);
                })
                .catch(function(error){
                    $log.error(error);
                });
        });

        $scope.loadData = function(){
            if (!$scope.hasMore){
                $scope.$broadcast('scroll.infiniteScrollComplete');
                return $q.when([]);
            }

            return activityHistoryService.getList($scope.lastId)
                .then(function(result){
                    $scope.lastId = result.lastId;

                    var lastItems = result.docs.map(function(historyItem){
                        var trainingDateFormatted = dateTimeService.toLongDate(historyItem.historyDate);
                        var duration = dateTimeService.toDuration(historyItem.duration);

                        return  {date: trainingDateFormatted, exerciseCount: historyItem.exerciseCount, duration: duration, id: historyItem._id};
                    });

                    $scope.historyItems = [].concat($scope.historyItems, lastItems);
                    $scope.hasMore = lastItems.length > 0;
                })
                .then(function(result){
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                    return result;
                });
        };

        $scope.goToDetails = function(id){
            $state.go('tab.history-details', { id: id })
        }

        self.init = function(){
            return $scope.loadData()
                .then(function(){
                    $log.log($scope.historyItems);
                })
                .catch(function(error){
                    $log.error(error);
                });
        };
    }]);
angular.module('training-app.controllers')
    .controller('HistoryDetailsController', ['$scope', '$log', '$q', '$stateParams', 'ActivityHistoryService', 'ExerciseService', 'DateTimeService', 'SUCCESS_RATES', function($scope, $log, $q, $stateParams, activityHistoryService, exerciseService, dateTimeService, SUCCESS_RATES) {
        var self = this;

        $scope.id = null;
        $scope.successRates = SUCCESS_RATES;
        $scope.history = null;

        $scope.$on("$ionicView.loaded", function(){
            $scope.id = $stateParams.id;
            $scope.variations = [];

            self.init()
                .then(function(){
                    return exerciseService.getExerciseVariations()
                        .then(function(variations){
                            $scope.variations = variations;
                        });
                })
                .catch(function(error){
                    $log.error(error);
                });
        });

        $scope.loadData = function(){
            return activityHistoryService.get($scope.id)
                .then(function(result){
                    var trainingDateFormatted = dateTimeService.toLongDate(result.historyDate);
                    var duration = dateTimeService.toDuration(result.duration);

                    $scope.history =  {date: trainingDateFormatted, exerciseCount: result.exerciseCount, duration: duration, id: result._id, completedExercises: []};

                    angular.forEach(result.completedExercises, function(exercise){
                        var singleExerciseDuration = dateTimeService.toDuration(exercise.duration);
                        var historyExercise = {id: exercise._id, duration: singleExerciseDuration, rating: $scope.successRates[exercise.rating.rating1][exercise.rating.rating2]};
                        $scope.history.completedExercises.push(historyExercise);
                    });

                    return result;
                });
        };

        self.init = function(){
            return $scope.loadData()
                .then(function(){
                    $log.log($scope.history);
                });
        };
    }]);
angular.module('training-app.controllers')
    .controller('ExercisesSummaryController', ['$scope', '$state', '$log', 'ExerciseService', function($scope, $state, $log, exerciseService) {
        var self = this;

        $scope.showDelete = false;
        $scope.showReorder = false;
        $scope.hasData = false;
        $scope.initialized = false;
        $scope.items = [];
        $scope.variations = [];

        $scope.addRemoveExercises = function(){
            $state.go("tab.exercises");
        };

        $scope.$on("$ionicView.loaded", function(){
            self.init(true);
        });

        $scope.toggleDelete = function(){
            $scope.showDelete = !$scope.showDelete;
        };

        $scope.toggleReorder = function(){
            $scope.showReorder = !$scope.showReorder;
        };

        $scope.saveChanges = function(){
            exerciseService.saveExerciseSummaryExercises($scope.items)
                .then(self.init)
                .then(self.resetActionButtonVisibility)
                .catch(function(error){
                    $log.error(error);
                });
        };

        $scope.cancelChanges = function(){
            self.init()
                .then(self.resetActionButtonVisibility)
                .catch(function(error){
                    $log.error(error);
                });
        };

        $scope.moveItem = function(item, fromIndex, toIndex) {
            $scope.items.splice(fromIndex, 1);
            $scope.items.splice(toIndex, 0, item);
        };

        $scope.onItemDelete = function(item) {
            $scope.items.splice($scope.items.indexOf(item), 1);
        };

        self.init = function(loadVariations){
            return exerciseService.getExerciseSummary()
                .then(function(summary){
                    $scope.items = summary.exercises;
                    $scope.hasData = $scope.items > [];
                    $scope.initialized = true;
                })
                .then(function(){
                    if (loadVariations){
                        exerciseService.getExerciseVariations()
                            .then(function(variations){
                                $scope.variations = variations;
                            });
                    }
                });
        };

        self.resetActionButtonVisibility = function() {
            $scope.showReorder = false;
            $scope.showDelete = false;
            $scope.hasData = $scope.items > [];
        };
    }]);