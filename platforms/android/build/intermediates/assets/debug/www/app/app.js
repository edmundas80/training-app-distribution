angular.module('training-app', ['training-app.controllers', 'training-app.services', 'training-app.directives', 'ionic', 'ngCookies', 'pascalprecht.translate', 'pouchdb'])

    .config(['$translateProvider', '$stateProvider', '$urlRouterProvider', '$ionicConfigProvider', function ($translateProvider, $stateProvider, $urlRouterProvider, $ionicConfigProvider){
        // ionic
        $ionicConfigProvider.tabs.position('bottom');
        $ionicConfigProvider.views.maxCache(1);

        // routing
        $stateProvider
            .state('tab', {
                url: '/tab',
                abstract: true,
                templateUrl: 'app/views/application-tabs.html'
            })
            .state('splash', {
                url: '/splash',
                templateUrl: 'app/views/splash.html',
                cache: false
            })
            .state('tab.home', {
                url: '/home',
                views: {
                    'tab-home': {
                        templateUrl: 'app/views/home.html',
                        controller: 'HomeController'
                    }
                },
                cache: false
            })
            .state('tab.exercises-summary', {
                url: '/exercises-summary',
                views: {
                    'tab-exercises-summary': {
                        templateUrl: 'app/views/exercises-summary.html',
                        controller: 'ExercisesSummaryController'
                    }
                },
                cache: false
            })
            .state('tab.exercises', {
                url: '/exercises',
                views: {
                    'tab-exercises-summary': {
                        templateUrl: 'app/views/exercises-tabs.html',
                        controller: 'ExerciseTabsController'
                    }
                },
                cache: false
            })
            .state('tab.exercises.level-1', {
                url: '/level-1',
                views: {
                    'tab-exercises-level1': {
                        templateUrl: 'app/views/exercises.html',
                        controller: 'ExerciseListController'
                    }
                },
                params : { level: 1 },
                cache: false
            })
            .state('tab.exercises.level-2', {
                url: '/level-2',
                views: {
                    'tab-exercises-level2': {
                        templateUrl: 'app/views/exercises.html',
                        controller: 'ExerciseListController'
                    }
                },
                params : { level: 2 },
                cache: false
            })
            .state('tab.exercises.level-3', {
                url: '/level-3',
                views: {
                    'tab-exercises-level3': {
                        templateUrl: 'app/views/exercises.html',
                        controller: 'ExerciseListController'
                    }
                },
                params : { level: 3 },
                cache: false
            })
            .state('tab.history', {
                url: '/history',
                views: {
                    'tab-history': {
                        templateUrl: 'app/views/history.html',
                        controller: 'HistoryController'
                    }
                },
                cache: false
            });

        $urlRouterProvider.otherwise('/splash');

        // translations
        $translateProvider
            .useStaticFilesLoader({
                prefix: 'translations/locale-',
                suffix: '.json'
            })
            .useSanitizeValueStrategy('escape')
            .preferredLanguage('lt')
            .useLocalStorage();
    }])
    .run(['$q', '$log', '$ionicPlatform', 'DatabaseMigration', '$state', '$ionicHistory', function($q, $log, $ionicPlatform, migrationService, $state, $ionicHistory) {
        $ionicPlatform.ready(function() {
            if(window.cordova && window.cordova.plugins.Keyboard) {
              // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
              // for form inputs)
              cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

              // Don't remove this line unless you know what you are doing. It stops the viewport
              // from snapping when text inputs are focused. Ionic handles this internally for
              // a much nicer keyboard experience.
              cordova.plugins.Keyboard.disableScroll(true);
            }

            if(window.StatusBar) {
              StatusBar.styleDefault();
            }

            migrationService.executeDbMigration()
                .then($ionicHistory.clearCache)
                .then($ionicHistory.clearHistory)
                .then(function(){
                    $ionicHistory.nextViewOptions({
                        disableAnimate: true,
                        disableBack: true,
                        historyRoot: true
                    });

                    $state.go('tab.home');
                })
                .catch(function(error){
                    $log.error(error);
                });
        });
}]);
