angular.module('training-app.controllers')
    .value("ACTIVITY_TRAINING_NOT_STARTED", 0)
    .value("ACTIVITY_TRAINING_IN_PROGRESS", 1)
    .value("ACTIVITY_TRAINING_COMPLETED", 2)
    .controller('HomeController', ['$scope', '$rootScope', '$ionicPlatform', '$ionicPopover', '$log', '$timeout', '$document', 'ModalService', 'SUCCESS_RATES', 'ExerciseService', 'ActivityService', 'Repository', 'ACTIVITY_TRAINING_NOT_STARTED', 'ACTIVITY_TRAINING_IN_PROGRESS', 'ACTIVITY_TRAINING_COMPLETED', function($scope, $rootScope, $ionicPlatform, $ionicPopover, $log, $timeout, $document, modalService, SUCCESS_RATES, exerciseService, activityService, repository, ACTIVITY_TRAINING_NOT_STARTED, ACTIVITY_TRAINING_IN_PROGRESS, ACTIVITY_TRAINING_COMPLETED) {
        $scope.successRates =  SUCCESS_RATES;

        $scope.startedAfterInit = false;
        $scope.isPlayOn = false;
        $scope.isStopOn = true;
        $scope.isPauseOn = false;
        $scope.mode = ACTIVITY_TRAINING_NOT_STARTED;
        $scope.modeStr = "";
        $scope.variations = {};
        $scope.total = -1;
        $scope.current = 0;
        $scope.startTime = new Date();

        $scope.$watch("mode", function(value){
            if (value == ACTIVITY_TRAINING_NOT_STARTED){
                $scope.modeStr = "ACTIVITY_TRAINING_NOT_STARTED";
            } else if (value == ACTIVITY_TRAINING_IN_PROGRESS){
                $scope.modeStr = "ACTIVITY_TRAINING_IN_PROGRESS";
            } else {
                $scope.modeStr = "ACTIVITY_TRAINING_COMPLETED";
            }
        }, true);

        $scope.$on("$ionicView.loaded", function(){
            self.init()
                .catch(function(error){
                    $log.error(error);
                });
        });

        var stateChangeListener = $rootScope.$on('$stateChangeStart', function(){
            $log.log('$stateChangeStart');

            activityService.getActivity()
                .then(function(activity){
                    if (activity.inProgress && !activity.suspended){
                        return activityService.suspendActivity(true);
                    }
                })
                .then(function(){
                    $scope.$broadcast('timer-stop');
                })
                .catch(function(error){
                    $log.error(error);
                });

            stateChangeListener()
        });

        var pauseListener = $ionicPlatform.on('pause', function() {
            activityService.getActivity()
                .then(function(activity){
                    if (activity.inProgress && !activity.suspended){
                        return activityService.suspendActivity(true);
                    }
                })
                .then(function(){
                    $scope.$broadcast('timer-stop');
                })
                .catch(function(error){
                    $log.error(error);
                });
        });

        var resumeListener = $ionicPlatform.on('resume', function() {
            activityService.getActivity()
                .then(function(activity){
                    if (activity.suspended && activity.autostart){
                        return $scope.play();
                    }
                })
                .catch(function(error){
                    $log.error(error);
                });
        });

        $scope.$on('$destroy', function() {
            resumeListener();
            pauseListener();

            $scope.popover.remove();
        });

        $scope.play = function(){
            $scope.isPlayOn = true;
            $scope.isStopOn = $scope.isPauseOn = false;

            activityService.startActivity()
                .then(function(activity){
                    self.setStartTime(activity);
                    $scope.$broadcast('timer-resume');
                    $scope.mode = ACTIVITY_TRAINING_IN_PROGRESS;
                    $scope.startedAfterInit = true;
                })
                .catch(function(error){
                    $log.error(error);
                });
        }

        $scope.stop = function(){
            $scope.isStopOn = true;
            $scope.isPlayOn = $scope.isPauseOn = false;

            activityService.stopActivity()
                .then(self.getNextActivity)
                .then(function(){
                    $scope.$broadcast('timer-stop');
                    modalService.showModal('app/views/rating-modal.html', $scope);
                })
                .catch(function(error){
                    $log.error(error);
                });
        }

        $scope.pause = function(){
            $scope.isPauseOn = true;
            $scope.isStopOn = $scope.isPlayOn = false;

            activityService.suspendActivity()
                .then(function(){
                    $scope.$broadcast('timer-stop');
                })
                .catch(function(error){
                    $log.error(error);
                })
        }

        $scope.revertToPreviousExercise = function(){
            activityService.revertActivity()
                .then(function(activity){
                    $scope.currentExercise = activity.currentExercise;
                })
                .then($scope.play)
                .then(function(){
                    $scope.hideModal();
                })
                .catch(function(error){
                    $log.error(error);
                });
        }

        $scope.selectSuccessRate = function($event, index1, index2) {
            var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
            var successRateElement = angular.element($event.currentTarget).children(':first');

            successRateElement.addClass("animated bounce").one(animationEnd, function() {
                successRateElement.removeClass('animated bounce');

                activityService.rateLastActivity(index1, index2)
                    .then(function(){
                        $scope.hideModal();
                        $scope.current++;

                        if ($scope.total == $scope.current){
                            $scope.mode = ACTIVITY_TRAINING_COMPLETED;

                            $timeout(function(){
                                modalService.showModal('app/views/training-completed-modal.html', $scope);
                            }, 3000);
                        }
                    })
                    .catch(function(error){
                        $log.error(error);
                    });
            });
        };

        $scope.newTraining = function(){
            activityService.getActivity()
                .then(function(activity){
                    return activityService.removeActivity();
                })
                .then(function(){
                    return self.init();
                })
                .catch(function(error){
                    $log.error(error);
                })
                .finally(function(){
                    $scope.popover.hide();
                    $scope.startedAfterInit = false;
                });
        };

        $scope.openMenu = function($event){
            $log.log('click', $event);
            $scope.popover.show($event);
        };

        self.init = function(){
            if (!$scope.popover){
                $ionicPopover.fromTemplateUrl('home-menu', {
                    scope: $scope
                }).then(function(popover) {
                    $scope.popover = popover;
                });
            }

            $log.log("Initialize activity data.");

            return self.getNextActivity()
                .then(exerciseService.getExerciseVariations)
                .then(function(variations){
                    $scope.variations = variations;
                })
                .then(activityService.getTotalExercisesForActivity)
                .then(function(result){
                    var activity = result.activity;

                    self.setStartTime(activity);

                    $scope.current = activity.completedExercises.length;
                    $scope.total = result.total;

                    $log.log('Current exercise', $scope.currentExercise);
                    $log.log('Variations', $scope.variations);

                    var autostart = activity.autostart || (activity.inProgress && !activity.suspended);

                    if (autostart){
                        $scope.play();
                    }

                    $scope.isPauseOn = activity.suspended && !autostart;

                    if ($scope.total == $scope.current){
                        $scope.mode = ACTIVITY_TRAINING_COMPLETED;
                    } else if (activity.started){
                        $scope.mode = ACTIVITY_TRAINING_IN_PROGRESS;
                    } else {
                        $scope.mode = ACTIVITY_TRAINING_NOT_STARTED;
                    }
                });
        };

        self.getNextActivity = function(){
            return activityService.getActivity()
                .then(function(activity){
                    $log.log("Activity data received.");
                    $scope.currentExercise = activity.currentExercise;
                    return activity;
                });
        }

        self.setStartTime = function(activity){
            var i;
            var completedExerciseDuration = 0;

            for (i = 0; i < activity.completedExercises.length; i++){
                completedExerciseDuration += activity.completedExercises[i].duration;
            }

            if (activity.autostart){
                $scope.startTime = new Date() - (moment().unix() - activity.lastStartTime + activity.duration + completedExerciseDuration) * 1000;
            } else {
                $scope.startTime = new Date() - (activity.duration + completedExerciseDuration) * 1000;
            }
        }
    }]);