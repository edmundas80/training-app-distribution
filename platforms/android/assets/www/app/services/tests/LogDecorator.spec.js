describe('LogDecorator', function() {
    beforeEach(function() {
        module('training-app.services');

        module(function($provide){
            $provide.decorator("$window", function($delegate){
                $delegate.ionic = {Platform: {isWebView: function() { }}};
                return $delegate;
            })
        });
    });

    it('error on non-mobile', function(done) {
        inject(function ($log, $window) {
            $log.reset();

            spyOn($window.ionic.Platform, "isWebView").and.returnValue(false);
            spyOn($window, "alert");

            $log.error("test");

            expect($window.alert).not.toHaveBeenCalled();
            done();
        })
    });

    it('error on mobile', function(done) {
        inject(function ($log, $window) {
            $log.reset();

            spyOn($window.ionic.Platform, "isWebView").and.returnValue(true);
            spyOn($window, "alert");

            $log.error("test");

            expect($window.alert).toHaveBeenCalledWith("test");
            done();
        })
    });
});