angular.module('training-app.services')
.config(function($provide){
    $provide.decorator("$log", function($delegate) {
        var errorFn = $delegate.error;
        $delegate.error = function(message) {
            if (ionic.Platform.isWebView()){
                alert(message);
            }

            errorFn.apply(null, arguments);
        };
        return $delegate;
    });
});