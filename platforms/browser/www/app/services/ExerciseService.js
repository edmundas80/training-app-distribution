
angular.module('training-app.services')
    .constant('EXERCISE_DRAFT_DOC_ID', '9bbcc844-0376-4f40-b4ab-c34cc048775c')
    .constant('EXERCISE_SUMMARY_DOC_ID', '35b623a1-7fd3-4543-928a-d2a69303949e')
    .service('ExerciseService', ['Repository', '$q', 'filterFilter', '$filter', 'EXERCISE_DRAFT_DOC_ID', 'EXERCISE_SUMMARY_DOC_ID', function(repository, $q, filterFilter, $filter, EXERCISE_DRAFT_DOC_ID, EXERCISE_SUMMARY_DOC_ID){
        var self = this;
        var variations = null;

        self.getExercises = function(level){
            return repository.all()
                .then(function(docs){
                    var filter = {type: 'exercise'};

                    if (level){
                        filter.stage = level;
                    }

                    return filterFilter(docs, filter);
                })
                .then(function(exercises){
                    return $filter('orderBy')(exercises, 'name');
                });
        };

        self.getExerciseVariations = function(){
            if (variations){
                return $q.when(variations);
            }

            return repository.all()
                .then(function(exercises){
                    var result = [];

                    angular.forEach(exercises, function(exercise){
                        angular.forEach(exercise.variations, function(variation){
                            var e = angular.copy(exercise);
                            e.productName = e.name;
                            e.name = variation.name;
                            e.description = variation.description;
                            delete e.variations;
                            result[variation._id] = e;
                        });
                    });

                    variations = result;
                    return result;
                });
        };

        self.getExerciseConfigurationDraft = function(){
            return repository.getSafe(EXERCISE_DRAFT_DOC_ID, self._createExerciseConfigurationDraft)
                .then(function(doc){
                    return doc;
                });
        };

        self.saveExerciseConfigurationDraft = function(exerciseVariations, level){
            if (!exerciseVariations){
                return $q.reject("exerciseVariations is not specified");
            }

            if (level == null || !(level >= 1 && level <= 3)){
                return $q.reject("level is invalid or not specified");
            }

            var variations = exerciseVariations.map(function(variation) {
                return variation._id;
            });

            return repository.getSafe(EXERCISE_DRAFT_DOC_ID, self._createExerciseConfigurationDraft)
                .then(function(doc){
                    doc.level[level - 1] = variations;
                    doc.dirty = true;

                    return repository.saveOrUpdate(doc);
                });
        };

        self.removeExerciseConfigurationDraft = function(){
            return repository.getSafe(EXERCISE_DRAFT_DOC_ID, function(){ return null; })
                .then(function(doc){
                    if (doc){
                        return repository.db.remove(EXERCISE_DRAFT_DOC_ID, doc._rev);
                    }
                });
        };

        self.getExerciseSummary = function(){
            return repository.getSafe(EXERCISE_SUMMARY_DOC_ID, self._createExerciseSummary)
                .then(function(doc){
                    return doc;
                });
        };

        self.saveExerciseSummaryFromDraft = function(draft){
            if (!draft || !draft.level){
                return $q.reject("invalid draft specified");
            }

            return repository.getSafe(EXERCISE_SUMMARY_DOC_ID, self._createExerciseSummary)
                .then(function(doc){
                    var draftExercises = [].concat([].concat(draft.level[0], draft.level[1]), draft.level[2]);
                    var storedExercises = doc.exercises;
                    var index;

                    // Summary exercise does not exists in draft - delete it from summary, it is not configured anymore
                    for (index = storedExercises.length - 1; index >= 0 ; index--){
                        if (draftExercises.indexOf(storedExercises[index]) == -1){
                            storedExercises.splice(index, 1);
                        }
                    }

                    // Draft exercise does exist is summary - delete it from draft to maintain summary exercise position
                    for (index = draftExercises.length - 1; index >= 0 ; index--){
                        if (storedExercises.indexOf(draftExercises[index]) !== -1){
                            draftExercises.splice(index, 1);
                        }
                    }

                    doc.exercises = [].concat(draftExercises, storedExercises);
                    return repository.saveOrUpdate(doc);
                });
        };

        self.saveExerciseSummaryExercises = function(exercises){
            return repository.getSafe(EXERCISE_SUMMARY_DOC_ID, self._createExerciseSummary)
                .then(function(doc){
                    doc.exercises = exercises;
                    return repository.saveOrUpdate(doc);
                });
        };

        self._deleteDatabase = function(){
            return repository.db.destroy();
        };

        self._createExerciseConfigurationDraft = function(){
            var result = {
                _id: EXERCISE_DRAFT_DOC_ID,
                type: "exerciseConfigurationDraft",
                level: [[], [], []],
                dirty: false
            };

            return repository.getSafe(EXERCISE_SUMMARY_DOC_ID, self._createExerciseSummary)
                .then(function(summaryDoc){
                    if (summaryDoc.exercises > []){
                        var summaryExercises = summaryDoc.exercises;
                        return self.getExerciseVariations()
                            .then(function(variations){
                                angular.forEach(summaryExercises, function(exercise){
                                    if (variations[exercise]){
                                        result.level[variations[exercise].stage - 1].push(exercise);
                                    }
                                });

                                return result;
                            });
                    }

                    return result;
                });
        };

        self._createExerciseSummary = function(){
            return {
                _id: EXERCISE_SUMMARY_DOC_ID,
                type: "exerciseSummary",
                exercises: []
            };
        };
    }]);