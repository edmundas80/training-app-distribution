describe('ExerciseService', function() {
    beforeEach(function() {
        module(function($provide){
            $provide.constant("DB_NAME", "TESTDB");
        });

        module('pouchdb');
        module('training-app.services');
    });

    it('getExercises without filter', function(done) {
        inject(function (pouchDB, ExerciseService, Repository, $rootScope, $q) {
            spyOn(Repository, "all").and.returnValue($q.when([{_id: "1", type: "exercise", name: "a1"}, {_id: "2", type: "other", name: "a2"}]));

            ExerciseService.getExercises()
                .then(function (docs) {
                    expect(docs.length).toBe(1);
                    expect(docs[0]._id).toBe("1");
                    expect(docs[0].type).toBe("exercise");
                    expect(Repository.all).toHaveBeenCalled();
                })
                .catch(function(error){
                    fail(error.message);
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('getExercises with filter', function(done) {
        inject(function (pouchDB, ExerciseService, Repository, $rootScope, $q) {
            spyOn(Repository, "all").and.returnValue($q.when([
                {_id: "1", type: "exercise", name: "a1", stage: 1},
                {_id: "2", type: "exercise", name: "a2", stage: 2},
                {_id: "3", type: "other", name: "a3", stage: 3}
            ]));

            ExerciseService.getExercises(2)
                .then(function (docs) {
                    expect(docs.length).toBe(1);
                    expect(docs[0]._id).toBe("2");
                    expect(docs[0].name).toBe("a2");
                    expect(Repository.all).toHaveBeenCalled();
                })
                .catch(function(error){
                    fail(error.message);
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('getExercises with filter ordered', function(done) {
        inject(function (pouchDB, ExerciseService, Repository, $rootScope, $q) {
            spyOn(Repository, "all").and.returnValue($q.when([
                {_id: "1", type: "exercise", name: "c1", stage: 2},
                {_id: "2", type: "exercise", name: "b2", stage: 2},
                {_id: "3", type: "exercise", name: "a3", stage: 2}
            ]));

            ExerciseService.getExercises(2)
                .then(function (docs) {
                    expect(docs.length).toBe(3);
                    expect(docs[0].name).toBe("a3");
                    expect(docs[1].name).toBe("b2");
                    expect(docs[2].name).toBe("c1");
                    expect(Repository.all).toHaveBeenCalled();
                })
                .catch(function(error){
                    fail(error.message);
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('getExerciseVariations', function(done) {
        inject(function (pouchDB, ExerciseService, Repository, $rootScope, $q) {
            spyOn(Repository, "all").and.returnValue($q.when([
                {_id: "2", type: "exercise", name: "c2", variations: [{_id: "v21", name: "v21", description: "d21"}, {_id: "v22", name: "v22", description: "d22"}]},
                {_id: "1", type: "exercise", name: "c1", variations: [{_id: "v11", name: "v11", description: "d11"}, {_id: "v12", name: "v12", description: "d12"}]}
            ]));

            ExerciseService.getExerciseVariations()
                .then(function (docs) {
                    expect(docs["v11"].name).toBe("v11");
                    expect(docs["v11"].productName).toBe("c1");
                    expect(docs["v11"].description).toBe("d11");
                    expect(docs["v11"].variations).toBeUndefined();
                    expect(Repository.all).toHaveBeenCalled();
                })
                .catch(function(error){
                    fail(error.message);
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('_createExerciseConfigurationDraft when there is no summary', function(done) {
        inject(function (pouchDB, ExerciseService, Repository, $rootScope, $q, EXERCISE_DRAFT_DOC_ID) {
            spyOn(Repository, "getSafe").and.returnValue($q.when(ExerciseService._createExerciseSummary()));

            ExerciseService._createExerciseConfigurationDraft()
                .then(function (doc) {
                    expect(doc._id).toBe(EXERCISE_DRAFT_DOC_ID);
                    expect(doc.type).toBe("exerciseConfigurationDraft");
                    expect(doc.level.length).toBe(3);
                    expect(doc.level[0].length).toBe(0);
                    expect(doc.level[1].length).toBe(0);
                    expect(doc.level[2].length).toBe(0);
                    expect(Repository.getSafe).toHaveBeenCalled();
                })
                .catch(function(error){
                    fail(error.message);
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('_createExerciseConfigurationDraft when there is a summary', function(done) {
        inject(function (pouchDB, ExerciseService, Repository, $rootScope, $q, EXERCISE_DRAFT_DOC_ID) {
            spyOn(Repository, "getSafe").and.returnValue($q.when((function(){
                var doc = ExerciseService._createExerciseSummary();
                doc.exercises = ["v11"];
                return doc;
            })()));

            spyOn(ExerciseService, "getExerciseVariations").and.returnValue($q.when((function(){
                var result = {};
                result["v11"] = {_id: "v11", name: "v11", description: "d11", productName: "p1", stage: 2};
                result["v12"] = {_id: "v12", name: "v12", description: "d12", productName: "p1", stage: 2};
                return result;
            })()));

            ExerciseService._createExerciseConfigurationDraft()
                .then(function (doc) {
                    expect(doc._id).toBe(EXERCISE_DRAFT_DOC_ID);
                    expect(doc.type).toBe("exerciseConfigurationDraft");
                    expect(doc.level.length).toBe(3);
                    expect(doc.level[0].length).toBe(0);
                    expect(doc.level[1].length).toBe(1);
                    expect(doc.level[1][0]).toBe("v11");
                    expect(doc.level[2].length).toBe(0);
                    expect(Repository.getSafe).toHaveBeenCalled();
                    expect(ExerciseService.getExerciseVariations).toHaveBeenCalled();
                })
                .catch(function(error){
                    fail(error.message);
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('saveExerciseConfigurationDraft invalid exerciseVariations', function(done) {
        inject(function (pouchDB, ExerciseService, Repository, $rootScope) {
            spyOn(Repository, "saveOrUpdate");

            ExerciseService.saveExerciseConfigurationDraft(null, 0)
                .catch(function (error) {
                    expect(error).toBe("exerciseVariations is not specified");
                    expect(Repository.saveOrUpdate).not.toHaveBeenCalled();
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('saveExerciseConfigurationDraft invalid exerciseVariations', function(done) {
        inject(function (pouchDB, ExerciseService, Repository, $rootScope) {
            spyOn(Repository, "saveOrUpdate");

            ExerciseService.saveExerciseConfigurationDraft(null, 0)
                .catch(function (error) {
                    expect(error).toBe("exerciseVariations is not specified");
                    expect(Repository.saveOrUpdate).not.toHaveBeenCalled();
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('saveExerciseConfigurationDraft invalid level', function(done) {
        inject(function (pouchDB, ExerciseService, Repository, $rootScope) {
            spyOn(Repository, "saveOrUpdate");

            ExerciseService.saveExerciseConfigurationDraft([], null)
                .catch(function (error) {
                    expect(error).toBe("level is invalid or not specified");
                    expect(Repository.saveOrUpdate).not.toHaveBeenCalled();
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('saveExerciseConfigurationDraft success', function(done) {
        inject(function (pouchDB, ExerciseService, Repository, $rootScope, $q, EXERCISE_DRAFT_DOC_ID) {
            spyOn(Repository, "saveOrUpdate");

            spyOn(Repository, "getSafe").and.returnValue($q.when((function(){
                return {
                    _id: EXERCISE_DRAFT_DOC_ID,
                    type: "exerciseConfigurationDraft",
                    level: [["a11", "a12"], [], []],
                    dirty: false
                };
            })()));

            spyOn(ExerciseService, "getExerciseVariations").and.returnValue($q.when((function(){
                var result = {};
                result["v11"] = {_id: "v11", name: "v11", description: "d11", productName: "p1", stage: 2};
                result["v12"] = {_id: "v12", name: "v12", description: "d12", productName: "p1", stage: 2};
                return result;
            })()));

            ExerciseService.saveExerciseConfigurationDraft([{_id: "v11"}], 2)
                .then(function () {
                    var doc = Repository.saveOrUpdate.calls.argsFor(0)[0];

                    expect(Repository.getSafe).toHaveBeenCalled();
                    expect(doc.level[0]).toEqual(["a11", "a12"]);
                    expect(doc.level[1]).toEqual(["v11"]);
                    expect(doc.level[2]).toEqual([]);
                    expect(doc.dirty).toBe(true);
                })
                .catch(function(error){
                    fail(error.message);
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('saveExerciseSummaryFromDraft success', function(done) {
        inject(function (pouchDB, ExerciseService, Repository, $rootScope, $q, EXERCISE_DRAFT_DOC_ID) {
            spyOn(Repository, "saveOrUpdate");

            spyOn(Repository, "getSafe").and.returnValue($q.when((function(){
                var doc = ExerciseService._createExerciseSummary();
                doc.exercises = ["a11", "v11"];
                return doc;
            })()));

            var draft = {
                _id: EXERCISE_DRAFT_DOC_ID,
                type: "exerciseConfigurationDraft",
                level: [["a10", "a11", "a12"], ["v12"], ["c13", "c14"]],
                dirty: false
            };

            ExerciseService.saveExerciseSummaryFromDraft(draft)
                .then(function () {
                    var doc = Repository.saveOrUpdate.calls.argsFor(0)[0];

                    expect(Repository.getSafe).toHaveBeenCalled();
                    expect(doc.exercises).toEqual(["a10", "a12", "v12", "c13", "c14", "a11"]);
                })
                .catch(function(error){
                    fail(error.message);
                })
                .finally(done);

            $rootScope.$digest();
        })
    });

    it('saveExerciseSummaryFromDraft invalid draft', function(done) {
        inject(function (pouchDB, ExerciseService, Repository, $rootScope, $q, EXERCISE_DRAFT_DOC_ID) {
            spyOn(Repository, "saveOrUpdate");
            spyOn(Repository, "getSafe");

            var draft = {
            };

            ExerciseService.saveExerciseSummaryFromDraft(draft)
                .catch(function(error){
                    expect(error).toBe("invalid draft specified");
                    expect(Repository.getSafe).not.toHaveBeenCalled();
                    expect(Repository.saveOrUpdate).not.toHaveBeenCalled();
                })
                .finally(done);

            $rootScope.$digest();
        })
    });
});