angular.module('training-app.services')
    .service('ModalService', ['$ionicModal', function($ionicModal){
        var self = this;

        self.showModal = function(templateUrl, scope){
            $ionicModal.fromTemplateUrl(templateUrl, {
                scope: scope,
                animation: 'animated bounceIn',
                hideDelay: 920,
                backdropClickToClose: false,
                hardwareBackButtonClose: false
            }).then(function(modal) {
                scope.modal = modal;
                scope.modal.show();
                scope.hideModal = function(){
                    if (scope.modal){
                        scope.modal.hide();
                        scope.modal.remove();
                        scope.modal = null;
                    }
                }
            });
        }
    }]);