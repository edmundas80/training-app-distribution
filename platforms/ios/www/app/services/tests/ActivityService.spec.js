describe('ActivityService', function() {
    beforeEach(function() {
        module(function($provide){
            $provide.constant("DB_NAME", "TESTDB");
            $provide.constant("moment", moment);
        });

        module('pouchdb');
        module('training-app.services');
    });

    describe('_setFirstAvailableExercise', function(){
        it('when no exercises completed', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                spyOn(ExerciseService, "getExerciseSummary").and.returnValue($q.when({exercises: ["1"]}));
                var activity = ActivityService._createActivity();

                ActivityService._setFirstAvailableExercise(activity)
                    .then(function (activity) {
                        expect(activity.completedExercises.length).toBe(0);
                        expect(activity.currentExercise).toBe("1");
                        expect(ExerciseService.getExerciseSummary).toHaveBeenCalled();
                    })
                    .catch(function(error){
                        fail(error.message);
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });

        it('when 1 exercise completed', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                spyOn(ExerciseService, "getExerciseSummary").and.returnValue($q.when({exercises: ["1", "2"]}));
                var activity = ActivityService._createActivity();
                activity.completedExercises = [{_id: "1"}];

                ActivityService._setFirstAvailableExercise(activity)
                    .then(function (activity) {
                        expect(activity.completedExercises.length).toBe(1);
                        expect(activity.currentExercise).toBe("2");
                        expect(ExerciseService.getExerciseSummary).toHaveBeenCalled();
                    })
                    .catch(function(error){
                        fail(error.message);
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });

        it('when 2 exercises completed', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                spyOn(ExerciseService, "getExerciseSummary").and.returnValue($q.when({exercises: ["1", "2", "3"]}));
                var activity = ActivityService._createActivity();
                activity.completedExercises = [{_id: "1"}, {_id: "2"}];

                ActivityService._setFirstAvailableExercise(activity)
                    .then(function (activity) {
                        expect(activity.completedExercises.length).toBe(2);
                        expect(activity.currentExercise).toBe("3");
                        expect(ExerciseService.getExerciseSummary).toHaveBeenCalled();
                    })
                    .catch(function(error){
                        fail(error.message);
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });

        it('when no summary', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                spyOn(ExerciseService, "getExerciseSummary").and.returnValue($q.when({exercises: []}));
                var activity = ActivityService._createActivity();
                activity.completedExercises = [{_id: "1"}, {_id: "2"}];

                ActivityService._setFirstAvailableExercise(activity)
                    .then(function (activity) {
                        expect(activity.completedExercises.length).toBe(2);
                        expect(activity.currentExercise).toBeNull();
                        expect(ExerciseService.getExerciseSummary).toHaveBeenCalled();
                    })
                    .catch(function(error){
                        fail(error.message);
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });
    });

    describe('getActivity', function(){
        it('when not in progress', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                var activity = ActivityService._createActivity();

                spyOn(ExerciseService, "getExerciseSummary").and.returnValue($q.when({exercises: ["1"]}));
                spyOn(Repository, "getSafe").and.returnValue($q.when(activity));

                ActivityService.getActivity()
                    .then(function (activity) {
                        expect(activity.completedExercises.length).toBe(0);
                        expect(activity.currentExercise).toBe("1");
                        expect(ExerciseService.getExerciseSummary).toHaveBeenCalled();
                    })
                    .catch(function(error){
                        fail(error.message);
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });

        it('when in progress', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                var activity = ActivityService._createActivity();
                activity.inProgress = true;

                spyOn(ExerciseService, "getExerciseSummary").and.returnValue($q.when({exercises: ["1"]}));
                spyOn(Repository, "getSafe").and.returnValue($q.when(activity));

                ActivityService.getActivity()
                    .then(function (activity) {
                        expect(activity.completedExercises.length).toBe(0);
                        expect(activity.currentExercise).toBeNull();
                        expect(ExerciseService.getExerciseSummary).not.toHaveBeenCalled();
                    })
                    .catch(function(error){
                        fail(error.message);
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });
    })

    describe('startActivity', function(){
        it('when current exercise is set', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                var activity = ActivityService._createActivity();
                activity.currentExercise = "1";

                spyOn(ActivityService, "getActivity").and.returnValue($q.when(activity));
                spyOn(Repository, "saveOrUpdate").and.returnValue($q.when());

                ActivityService.startActivity()
                    .then(function (activity) {
                        expect(activity.completedExercises.length).toBe(0);
                        expect(activity.currentExercise).toBe("1");
                        expect(activity.started).toBeTruthy();
                        expect(activity.inProgress).toBeTruthy();
                        expect(activity.lastStartTime).toBeGreaterThan(0);
                        expect(Repository.saveOrUpdate).toHaveBeenCalledWith(activity);
                    })
                    .catch(function(error){
                        fail(error.message);
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });

        it('when current exercise is not set', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                var activity = ActivityService._createActivity();
                activity.currentExercise = null;

                spyOn(ActivityService, "getActivity").and.returnValue($q.when(activity));
                spyOn(Repository, "saveOrUpdate").and.returnValue($q.when());

                ActivityService.startActivity()
                    .catch(function(error){
                        expect(error).toBe("no exercise to start");
                        expect(activity.completedExercises.length).toBe(0);
                        expect(activity.currentExercise).toBeNull();
                        expect(activity.started).toBeFalsy();
                        expect(activity.inProgress).toBeFalsy();
                        expect(activity.lastStartTime).toBe(0);
                        expect(Repository.saveOrUpdate).not.toHaveBeenCalled();
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });

        it('when suspended', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                var activity = ActivityService._createActivity();
                activity.currentExercise = "1";
                activity.suspended = true;

                spyOn(ActivityService, "getActivity").and.returnValue($q.when(activity));
                spyOn(ActivityService, "resumeActivity").and.returnValue($q.when(activity));
                spyOn(Repository, "saveOrUpdate").and.returnValue($q.when());

                ActivityService.startActivity()
                    .then(function (activity) {
                        expect(ActivityService.resumeActivity).toHaveBeenCalled();
                        expect(Repository.saveOrUpdate).toHaveBeenCalledWith(activity);
                    })
                    .catch(function(error){
                        fail(error.message);
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });

        it('when activity in progress', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                var activity = ActivityService._createActivity();
                activity.currentExercise = "1";
                activity.inProgress = true;
                activity.started = true;

                spyOn(ActivityService, "getActivity").and.returnValue($q.when(activity));
                spyOn(Repository, "saveOrUpdate").and.returnValue($q.when());

                ActivityService.startActivity()
                    .then(function(){
                        expect(activity.completedExercises.length).toBe(0);
                        expect(activity.currentExercise).toBe("1");
                        expect(activity.started).toBeTruthy();
                        expect(activity.inProgress).toBeTruthy();
                        expect(activity.lastStartTime).toBe(0);
                        expect(Repository.saveOrUpdate).toHaveBeenCalled();
                    })
                    .catch(function(error){
                        faile(error.message);
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });
    });

    describe('stopActivity', function(){
        it('when activity is not started', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                var activity = ActivityService._createActivity();
                activity.currentExercise = "1";

                spyOn(ActivityService, "getActivity").and.returnValue($q.when(activity));
                spyOn(Repository, "saveOrUpdate").and.returnValue($q.when());

                ActivityService.stopActivity()
                    .catch(function(error){
                        expect(error).toBe("activity is not started");
                        expect(activity.completedExercises.length).toBe(0);
                        expect(activity.currentExercise).toBe("1");
                        expect(activity.started).toBeFalsy();
                        expect(activity.inProgress).toBeFalsy();
                        expect(activity.lastStartTime).toBe(0);
                        expect(Repository.saveOrUpdate).not.toHaveBeenCalled();
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });

        it('when no current exercise set', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                var activity = ActivityService._createActivity();
                activity.currentExercise = null;
                activity.inProgress = true;
                activity.started = true;

                spyOn(ActivityService, "getActivity").and.returnValue($q.when(activity));
                spyOn(Repository, "saveOrUpdate").and.returnValue($q.when());

                ActivityService.stopActivity()
                    .catch(function(error){
                        expect(error).toBe("no current exercise set");
                        expect(activity.completedExercises.length).toBe(0);
                        expect(activity.currentExercise).toBeNull();
                        expect(activity.started).toBeTruthy();
                        expect(activity.inProgress).toBeTruthy();
                        expect(activity.lastStartTime).toBe(0);
                        expect(Repository.saveOrUpdate).not.toHaveBeenCalled();
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });

        it('when current exercise is already completed', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                var activity = ActivityService._createActivity();
                activity.currentExercise = "1";
                activity.inProgress = true;
                activity.started = true;
                activity.lastStartTime = moment().unix();
                activity.completedExercises = [{_id: "0"}, {_id: "1"}];

                spyOn(ActivityService, "getActivity").and.returnValue($q.when(activity));
                spyOn(Repository, "saveOrUpdate").and.returnValue($q.when());

                ActivityService.stopActivity()
                    .catch(function(error){
                        expect(error).toBe("current exercise is already completed");
                        expect(activity.completedExercises.length).toBe(2);
                        expect(activity.currentExercise).toBe("1");
                        expect(activity.started).toBeTruthy();
                        expect(activity.inProgress).toBeTruthy();
                        expect(activity.lastStartTime).toBeGreaterThan(0);
                        expect(Repository.saveOrUpdate).not.toHaveBeenCalled();
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });

        it('when success and first exercise stopped', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                var activity = ActivityService._createActivity();
                activity.currentExercise = "1";
                activity.inProgress = true;
                activity.started = true;
                activity.lastStartTime = moment().unix() - 1;
                activity.completedExercises = [];

                spyOn(ActivityService, "getActivity").and.returnValue($q.when(activity));
                spyOn(Repository, "saveOrUpdate").and.returnValue($q.when());

                ActivityService.stopActivity()
                    .then(function(activity){
                        expect(activity.completedExercises.length).toBe(1);
                        expect(activity.completedExercises[0]._id).toBe("1");
                        expect(activity.completedExercises[0].duration).toBeGreaterThan(0);
                        expect(activity.completedExercises[0].duration).toBeLessThanOrEqual(3);
                        expect(activity.currentExercise).toBeNull();
                        expect(activity.started).toBeTruthy();
                        expect(activity.inProgress).toBeFalsy();
                        expect(activity.lastStartTime).toBe(0);
                        expect(activity.duration).toBe(0);
                        expect(Repository.saveOrUpdate).toHaveBeenCalledWith(activity);
                    })
                    .catch(function(error){
                        fail(error.message);
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });

        it('when success and not first exercise stopped', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                var activity = ActivityService._createActivity();
                activity.currentExercise = "1";
                activity.inProgress = true;
                activity.started = true;
                activity.lastStartTime = moment().unix() - 1;
                activity.completedExercises = [{_id: "2", duration: 100}];

                spyOn(ActivityService, "getActivity").and.returnValue($q.when(activity));
                spyOn(Repository, "saveOrUpdate").and.returnValue($q.when());

                ActivityService.stopActivity()
                    .then(function(activity){
                        expect(activity.completedExercises.length).toBe(2);
                        expect(activity.completedExercises[1]._id).toBe("1");
                        expect(activity.completedExercises[1].duration).toBeGreaterThan(0);
                        expect(activity.completedExercises[1].duration).toBeLessThanOrEqual(3);
                        expect(activity.currentExercise).toBeNull();
                        expect(activity.started).toBeTruthy();
                        expect(activity.inProgress).toBeFalsy();
                        expect(activity.lastStartTime).toBe(0);
                        expect(activity.duration).toBe(0);
                        expect(Repository.saveOrUpdate).toHaveBeenCalledWith(activity);
                    })
                    .catch(function(error){
                        fail(error.message);
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });

        it('when success after suspend', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                var activity = ActivityService._createActivity();
                activity.currentExercise = "1";
                activity.inProgress = true;
                activity.suspended = true;
                activity.started = true;
                activity.lastStartTime = 0;
                activity.duration = 10;
                activity.completedExercises = [];

                spyOn(ActivityService, "getActivity").and.returnValue($q.when(activity));
                spyOn(Repository, "saveOrUpdate").and.returnValue($q.when());

                ActivityService.stopActivity()
                    .then(function(activity){
                        expect(activity.completedExercises.length).toBe(1);
                        expect(activity.completedExercises[0]._id).toBe("1");
                        expect(activity.completedExercises[0].duration).toBe(10);
                        expect(activity.currentExercise).toBeNull();
                        expect(activity.started).toBeTruthy();
                        expect(activity.inProgress).toBeFalsy();
                        expect(activity.suspended).toBeFalsy();
                        expect(activity.lastStartTime).toBe(0);
                        expect(activity.duration).toBe(0);
                        expect(Repository.saveOrUpdate).toHaveBeenCalledWith(activity);
                    })
                    .catch(function(error){
                        fail(error.message);
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });
    });

    describe('getTotalExercisesForActivity', function(){
        it('when success with different currentExercise and completedExercises', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                var activity = ActivityService._createActivity();
                activity.currentExercise = "5";
                activity.inProgress = true;
                activity.started = true;
                activity.completedExercises = [{_id: "3"}, {_id: "4"}];

                spyOn(ExerciseService, "getExerciseSummary").and.returnValue($q.when({exercises: ["1", "2", "3"]}));
                spyOn(ActivityService, "getActivity").and.returnValue($q.when(activity));

                ActivityService.getTotalExercisesForActivity()
                    .then(function(result){
                        expect(result.total).toBe(5);
                        expect(result.activity).toEqual(activity);
                    })
                    .catch(function(error){
                        fail(error.message);
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });

        it('when success with null currentExercise and empty completedExercises', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                var activity = ActivityService._createActivity();
                activity.currentExercise = null;
                activity.completedExercises = [];

                spyOn(ExerciseService, "getExerciseSummary").and.returnValue($q.when({exercises: ["1", "2", "3"]}));
                spyOn(ActivityService, "getActivity").and.returnValue($q.when(activity));

                ActivityService.getTotalExercisesForActivity()
                    .then(function(result){
                        expect(result.total).toBe(3);
                        expect(result.activity).toEqual(activity);
                    })
                    .catch(function(error){
                        fail(error.message);
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });
    });

    describe('revertActivity', function(){
        it('when success', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                var activity = ActivityService._createActivity();
                activity.currentExercise = "5";
                activity.inProgress = false;
                activity.started = true;
                activity.completedExercises = [{_id: "3", duration: 20}, {_id: "4", duration: 10}];

                spyOn(ActivityService, "getActivity").and.returnValue($q.when(activity));
                spyOn(Repository, "saveOrUpdate").and.returnValue($q.when());

                ActivityService.revertActivity()
                    .then(function(activity){
                        expect(activity.inProgress).toBeFalsy();
                        expect(activity.started).toBeTruthy();
                        expect(activity.duration).toBe(10);
                        expect(activity.lastStartTime).toBe(0);
                        expect(activity.currentExercise).toBe("4");
                        expect(activity.completedExercises.length).toBe(1);
                        expect(Repository.saveOrUpdate).toHaveBeenCalledWith(activity);
                    })
                    .catch(function(error){
                        fail(error.message);
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });

        it('when activity in progress', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                var activity = ActivityService._createActivity();
                activity.inProgress = true;
                activity.completedExercises = [];

                spyOn(ActivityService, "getActivity").and.returnValue($q.when(activity));
                spyOn(Repository, "saveOrUpdate").and.returnValue($q.when());

                ActivityService.revertActivity()
                    .catch(function(error){
                        expect(error).toBe("activity is already started");
                        expect(Repository.saveOrUpdate).not.toHaveBeenCalled();
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });

        it('when no exercises completed', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                var activity = ActivityService._createActivity();
                activity.inProgress = false;
                activity.completedExercises = [];

                spyOn(ActivityService, "getActivity").and.returnValue($q.when(activity));
                spyOn(Repository, "saveOrUpdate").and.returnValue($q.when());

                ActivityService.revertActivity()
                    .catch(function(error){
                        expect(error).toBe("no completed exercises");
                        expect(Repository.saveOrUpdate).not.toHaveBeenCalled();
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });
    });

    describe('rateLastActivity', function(){
        it('when success', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                var activity = ActivityService._createActivity();
                activity.currentExercise = "5";
                activity.inProgress = false;
                activity.started = true;
                activity.completedExercises = [{_id: "3", duration: 20}, {_id: "4", duration: 10}];

                spyOn(ActivityService, "getActivity").and.returnValue($q.when(activity));
                spyOn(Repository, "saveOrUpdate").and.returnValue($q.when());

                ActivityService.rateLastActivity(1, 1)
                    .then(function(activity){
                        expect(activity.inProgress).toBeFalsy();
                        expect(activity.started).toBeTruthy();
                        expect(activity.completedExercises[1].rating).toEqual({rating1: 1, rating2: 1});
                        expect(Repository.saveOrUpdate).toHaveBeenCalledWith(activity);
                    })
                    .catch(function(error){
                        fail(error.message);
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });

        it('when no completed exercises ', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                var activity = ActivityService._createActivity();
                activity.completedExercises = [];

                spyOn(ActivityService, "getActivity").and.returnValue($q.when(activity));
                spyOn(Repository, "saveOrUpdate").and.returnValue($q.when());

                ActivityService.revertActivity()
                    .catch(function(error){
                        expect(error).toBe("no completed exercises");
                        expect(Repository.saveOrUpdate).not.toHaveBeenCalled();
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });

        it('when no exercises completed', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                var activity = ActivityService._createActivity();
                activity.completedExercises = [{_id: "1", duration: 10, ratring: {rating1: 1, rating2: 2}}];

                spyOn(ActivityService, "getActivity").and.returnValue($q.when(activity));
                spyOn(Repository, "saveOrUpdate").and.returnValue($q.when());

                ActivityService.revertActivity()
                    .catch(function(error){
                        expect(error).toBe("exercise is already rated");
                        expect(Repository.saveOrUpdate).not.toHaveBeenCalled();
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });
    });

    describe('suspendActivity', function(){
        it('when success first time suspended', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                var activity = ActivityService._createActivity();
                activity.currentExercise = "5";
                activity.inProgress = true;
                activity.lastStartTime = moment().unix() - 2;
                activity.started = true;
                activity.completedExercises = [{_id: "3", duration: 20}, {_id: "4", duration: 10}];

                spyOn(ActivityService, "getActivity").and.returnValue($q.when(activity));
                spyOn(Repository, "saveOrUpdate").and.returnValue($q.when());

                ActivityService.suspendActivity()
                    .then(function(activity){
                        expect(activity.inProgress).toBeTruthy();
                        expect(activity.started).toBeTruthy();
                        expect(activity.suspended).toBeTruthy();
                        expect(activity.duration).toBeGreaterThanOrEqual(2);
                        expect(activity.duration).toBeLessThanOrEqual(4);
                        expect(Repository.saveOrUpdate).toHaveBeenCalledWith(activity);
                    })
                    .catch(function(error){
                        fail(error.message);
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });

        it('when success next time suspended', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                var activity = ActivityService._createActivity();
                activity.currentExercise = "5";
                activity.inProgress = true;
                activity.lastStartTime = moment().unix() - 2;
                activity.started = true;
                activity.duration = 10;
                activity.completedExercises = [{_id: "3", duration: 20}, {_id: "4", duration: 10}];

                spyOn(ActivityService, "getActivity").and.returnValue($q.when(activity));
                spyOn(Repository, "saveOrUpdate").and.returnValue($q.when());

                ActivityService.suspendActivity()
                    .then(function(activity){
                        expect(activity.inProgress).toBeTruthy();
                        expect(activity.lastStartTime).toBe(0);
                        expect(activity.started).toBeTruthy();
                        expect(activity.suspended).toBeTruthy();
                        expect(activity.duration).toBeGreaterThanOrEqual(12);
                        expect(activity.duration).toBeLessThanOrEqual(14);
                        expect(Repository.saveOrUpdate).toHaveBeenCalledWith(activity);
                    })
                    .catch(function(error){
                        fail(error.message);
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });

        it('when success with autostart', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                var activity = ActivityService._createActivity();
                activity.currentExercise = "5";
                activity.inProgress = true;
                activity.lastStartTime = moment().unix() - 2;
                activity.started = true;
                activity.duration = 10;
                activity.completedExercises = [{_id: "3", duration: 20}, {_id: "4", duration: 10}];

                spyOn(ActivityService, "getActivity").and.returnValue($q.when(activity));
                spyOn(Repository, "saveOrUpdate").and.returnValue($q.when());

                ActivityService.suspendActivity(true)
                    .then(function(activity){
                        expect(activity.inProgress).toBeTruthy();
                        expect(activity.lastStartTime).toBeGreaterThan(0);
                        expect(activity.started).toBeTruthy();
                        expect(activity.suspended).toBeTruthy();
                        expect(activity.duration).toBeTruthy();
                        expect(Repository.saveOrUpdate).toHaveBeenCalledWith(activity);
                    })
                    .catch(function(error){
                        fail(error.message);
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });

        it('when activity not in progress', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                var activity = ActivityService._createActivity();
                activity.inProgress = false;

                spyOn(ActivityService, "getActivity").and.returnValue($q.when(activity));
                spyOn(Repository, "saveOrUpdate").and.returnValue($q.when());

                ActivityService.suspendActivity()
                    .catch(function(error){
                        expect(error).toBe("activity is not started");
                        expect(Repository.saveOrUpdate).not.toHaveBeenCalled();
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });

        it('when no current exercise', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                var activity = ActivityService._createActivity();
                activity.currentExercise = null;
                activity.inProgress = true;

                spyOn(ActivityService, "getActivity").and.returnValue($q.when(activity));
                spyOn(Repository, "saveOrUpdate").and.returnValue($q.when());

                ActivityService.suspendActivity()
                    .catch(function(error){
                        expect(error).toBe("no current exercise set");
                        expect(Repository.saveOrUpdate).not.toHaveBeenCalled();
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });

        it('when already suspended', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                var activity = ActivityService._createActivity();
                activity.currentExercise = "1";
                activity.inProgress = true;
                activity.suspended = true;

                spyOn(ActivityService, "getActivity").and.returnValue($q.when(activity));
                spyOn(Repository, "saveOrUpdate").and.returnValue($q.when());

                ActivityService.suspendActivity()
                    .catch(function(error){
                        expect(error).toBe("activity is already suspended");
                        expect(Repository.saveOrUpdate).not.toHaveBeenCalled();
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });
    });

    describe('resumeActivity', function(){
        it('when success', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                var activity = ActivityService._createActivity();
                activity.currentExercise = "5";
                activity.inProgress = true;
                activity.lastStartTime = 0
                activity.duration = 10;
                activity.started = true;
                activity.suspended = true;

                spyOn(ActivityService, "getActivity").and.returnValue($q.when(activity));
                spyOn(Repository, "saveOrUpdate").and.returnValue($q.when());

                ActivityService.resumeActivity()
                    .then(function(activity){
                        expect(activity.inProgress).toBeTruthy();
                        expect(activity.started).toBeTruthy();
                        expect(activity.suspended).toBeFalsy();
                        expect(activity.duration).toBe(10);
                        expect(activity.lastStartTime).toBeLessThanOrEqual(moment().unix());
                        expect(Repository.saveOrUpdate).toHaveBeenCalledWith(activity);
                    })
                    .catch(function(error){
                        fail(error.message);
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });

        it('when success after autostart', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                var activity = ActivityService._createActivity();
                activity.currentExercise = "5";
                activity.inProgress = true;
                activity.lastStartTime = moment().unix() - 10;
                activity.duration = 10;
                activity.started = true;
                activity.suspended = true;
                activity.autostart = true;

                spyOn(ActivityService, "getActivity").and.returnValue($q.when(activity));
                spyOn(Repository, "saveOrUpdate").and.returnValue($q.when());

                ActivityService.resumeActivity()
                    .then(function(activity){
                        expect(activity.inProgress).toBeTruthy();
                        expect(activity.started).toBeTruthy();
                        expect(activity.suspended).toBeFalsy();
                        expect(activity.autostart).toBeFalsy();
                        expect(activity.duration).toBeGreaterThan(10);
                        expect(activity.lastStartTime).toBeLessThanOrEqual(moment().unix());
                        expect(Repository.saveOrUpdate).toHaveBeenCalledWith(activity);
                    })
                    .catch(function(error){
                        fail(error.message);
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });

        it('when activity not in progress', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                var activity = ActivityService._createActivity();
                activity.inProgress = false;

                spyOn(ActivityService, "getActivity").and.returnValue($q.when(activity));
                spyOn(Repository, "saveOrUpdate").and.returnValue($q.when());

                ActivityService.resumeActivity()
                    .catch(function(error){
                        expect(error).toBe("activity is not started");
                        expect(Repository.saveOrUpdate).not.toHaveBeenCalled();
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });

        it('when no current exercise', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                var activity = ActivityService._createActivity();
                activity.currentExercise = null;
                activity.inProgress = true;

                spyOn(ActivityService, "getActivity").and.returnValue($q.when(activity));
                spyOn(Repository, "saveOrUpdate").and.returnValue($q.when());

                ActivityService.resumeActivity()
                    .catch(function(error){
                        expect(error).toBe("no current exercise set");
                        expect(Repository.saveOrUpdate).not.toHaveBeenCalled();
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });

        it('when not suspended', function(done) {
            inject(function (pouchDB, ActivityService, ExerciseService, Repository, $rootScope, $q, filterFilter) {
                var activity = ActivityService._createActivity();
                activity.currentExercise = "1";
                activity.inProgress = true;
                activity.suspended = false;

                spyOn(ActivityService, "getActivity").and.returnValue($q.when(activity));
                spyOn(Repository, "saveOrUpdate").and.returnValue($q.when());

                ActivityService.resumeActivity()
                    .catch(function(error){
                        expect(error).toBe("activity is not suspended");
                        expect(Repository.saveOrUpdate).not.toHaveBeenCalled();
                    })
                    .finally(done);

                $rootScope.$digest();
            })
        });
    });
});