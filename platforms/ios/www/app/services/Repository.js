angular.module('training-app.services')
    .factory('Repository', function(pouchDB, DB_NAME, $q){
        var db = pouchDB(DB_NAME);
        var repository = {};
        repository.db = db;

        repository.all = function(){
            return db.allDocs({include_docs: true})
                .then(function(docs){
                    return docs.rows.map(function(value){
                        return value.doc;
                    });
                });
        };

        repository.get = function(id){
            if (!id){
                return $q.reject("id is not specified");
            }

            return db.get(id);
        };

        repository.getSafe = function(id, createDocFn){
            if (!id){
                return $q.reject("id is not specified");
            }

            if (!createDocFn){
                return $q.reject("createDocFn is not specified");
            }

            return repository.exists(id)
                .then(function(exists){
                    if (exists){
                        return db.get(id);
                    }

                    return createDocFn();
                });
        };

        repository.exists = function(id){
            if (!id){
                return $q.reject("id is not specified");
            }

            return db.get(id)
                .then(function(doc){
                    return !!doc;
                })
                .catch(function(error){
                    if (error && error.error && error.message == 'missing'){
                        return false;
                    }

                    throw error;
                });
        };

        repository.saveOrUpdate = function(doc){
            return $q.when(doc)
                .then(function(doc){
                    if (!doc || !doc._id) {
                        return $q.reject("No document with _id");
                    }

                    return doc;
                })
                .then(function(doc){
                    return db.get(doc._id)
                        .then(function(docFromDb){
                            doc._rev = docFromDb._rev;
                            return db.put(doc);
                        });
                })
                .catch(function(error){
                    if (error && error.error && error.message == 'missing'){
                        return db.put(doc);
                    }

                    throw error;
                });
        };

        repository.remove = function(id){
            if (!id){
                return $q.reject("id is not specified");
            }

            return repository.get(id)
                .then(function(doc){
                    return db.remove(doc);
                });
        };

        return repository;
    });
