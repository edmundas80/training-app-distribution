angular.module('training-app.directives')
    .directive('tooltip', function($ionicPopover) {
        return {
            restrict: 'E',
            replace: true,
            template: '<a href="" class="padding-right" style="pointer-events: auto"></a>',
            scope: {
                text: '=',
                iconOnAttr: '@iconOn',
                iconOffAttr: '@iconOff'
            },
            link: function(scope, element, attributes){
                scope.iconOn = scope.iconOnAttr || "ion-ios-information";
                scope.iconOff = scope.iconOffAttr || "ion-ios-information-outline";

                var template = '<ion-popover-view>' +
                    '     <ion-header-bar> ' +
                    '          <h1 class="title">{{"HELP_ADDITIONAL_INFO" | translate }}</h1> '+
                    '             <div class="buttons"> '+
                    '                 <close-button ng-click="close()"></close-button>' +
                    '             </div>' +
                    '     </ion-header-bar> ' +
                    '     <ion-content class="padding"> {{text}} </ion-content>' +
                    '</ion-popover-view>';
                var popover = $ionicPopover.fromTemplate(template, {
                    scope: scope
                });

                element.addClass(scope.iconOff);
                element.on('click', function(event) {scope.open(event);});

                scope.close = function(){
                    element.removeClass(scope.iconOn);
                    element.addClass(scope.iconOff);

                    popover.hide();
                };

                scope.open = function(event){
                    element.removeClass(scope.iconOff);
                    element.addClass(scope.iconOn);

                    popover.show(event);
                };

                scope.$on('popover.hidden', function() {
                    element.removeClass(scope.iconOn);
                    element.addClass(scope.iconOff);
                });
            }
        };
    });