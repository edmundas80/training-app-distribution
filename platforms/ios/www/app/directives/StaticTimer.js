angular.module('training-app.directives')
    .directive('staticTimer', function(moment) {
        return {
            restrict: 'E',
            replace: true,
            template: '<span></span>',
            scope: {
                startTime: '='
            },
            link: function(scope, element){
                scope.$watch('startTime', function(newValue) {
                    if (newValue){
                        var millis = moment().diff(moment(newValue));

                        var seconds = Math.floor((millis / 1000) % 60);
                        var minutes = Math.floor(((millis / (60000)) % 60));
                        var hours = Math.floor(millis / 3600000);

                        var sseconds = seconds < 10 ? '0' + seconds : '' + seconds;
                        var mminutes = minutes < 10 ? '0' + minutes : '' + minutes;
                        var hhours = hours < 10 ? '0' + hours : '' + hours;

                        element.text(hhours + ':' + mminutes + ':' + sseconds);
                    }
                }, true);
            }
        };
    });